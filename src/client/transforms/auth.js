import {createTransform} from 'redux-persist';
import produce from 'immer';

const AuthTransform = createTransform(
    produce((draft) => {
        if (!draft.rememberMe) {
            delete draft.token;
            delete draft.user;
        }
    }),
    (outboundState) => outboundState,
    {whitelist: ['auth']}
);

export default AuthTransform;
