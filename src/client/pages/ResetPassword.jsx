/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {TextField, Grid, Link} from '@material-ui/core';
import {Link as RouterLink} from 'react-router-dom';
import {isResetPasswordLoading} from '../selectors';
import {loadResetPassword} from '../actions/auth';
import {setTitle} from '../actions/main';
import SubmitButton from '../components/SubmitButton';
import FormContainer from '../components/FormContainer';

class ResetPassword extends React.Component {
    static propTypes = {
        loading: PropTypes.bool.isRequired,

        loadResetPassword: PropTypes.func.isRequired,
        setTitle: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            email: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.props.setTitle('Reset your password');
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.loadResetPassword(this.state.email);
    }

    handleChange(event) {
        const {target} = event;
        const {name, value} = target;
        this.setState({[name]: value});
    }

    render() {
        const {email} = this.state;
        return (
            <FormContainer title="Reset password" onSubmit={this.handleSubmit}>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                    value={email}
                    onChange={this.handleChange}
                />
                <SubmitButton title="Send reset password email" loading={this.props.loading} />
                <Grid container>
                    <Grid item xs>
                        <Link component={RouterLink} to="/login" variant="body2">
                            Ooh, I just remembered my password!
                        </Link>
                    </Grid>
                    <Grid item>
                        <Link component={RouterLink} to="/register" variant="body2">
                            Dont have an account? Sign Up
                        </Link>
                    </Grid>
                </Grid>
            </FormContainer>
        );
    }
}

const mapStateToProps = (state) => ({
    loading: isResetPasswordLoading(state)
});

const actionCreators = {
    setTitle,
    loadResetPassword
};

export default connect(mapStateToProps, actionCreators)(ResetPassword);
