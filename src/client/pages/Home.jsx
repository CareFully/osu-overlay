import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Typography, withStyles} from '@material-ui/core';
import {compose} from 'redux';
import {
    isFooLoading, getFoo, getStreamCompanionStatus, getObsStatus
} from '../selectors';
import {loadFoo, setTitle} from '../actions/main';
import PageContainer from '../components/PageContainer';
import ConnectionStatus from '../components/ConnectionStatus';
import StreamCompanion from '../components/overlay/StreamCompanion';
import Obs from '../components/overlay/Obs';

class Home extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,

        streamCompanionStatus: PropTypes.number.isRequired,
        obsStatus: PropTypes.number.isRequired,

        setTitle: PropTypes.func.isRequired,
        loadFoo: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props.loadFoo();
        this.props.setTitle('Home');
    }

    render() {
        const {
            // eslint-disable-next-line no-unused-vars
            classes, streamCompanionStatus, obsStatus
        } = this.props;
        return (
            <PageContainer>
                <StreamCompanion />
                <Obs />
                <Typography variant="h5" component="h1" gutterBottom>
                    Welcome to Care osu! Overlay
                </Typography>
                <Typography variant="body1" gutterBottom>
                    Before starting a stream,
                    make sure all of the connections below are successful.
                </Typography>
                <ConnectionStatus name="Stream Companion" connectionStatus={streamCompanionStatus} />
                <ConnectionStatus name="OBS" connectionStatus={obsStatus} />
            </PageContainer>
        );
    }
}

const mapStateToProps = (state) => ({
    foo: getFoo(state),
    loading: isFooLoading(state),
    streamCompanionStatus: getStreamCompanionStatus(state),
    obsStatus: getObsStatus(state)
});

const actionCreators = {
    setTitle,
    loadFoo
};

export default compose(
    withStyles((/* theme */) => ({

    })),
    connect(mapStateToProps, actionCreators)
)(Home);
