/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {TextField} from '@material-ui/core';
import {
    isRegisterLoading
} from '../selectors';
import {loadRegister} from '../actions/user';
import {setTitle} from '../actions/main';
import FormContainer from '../components/FormContainer';
import SubmitButton from '../components/SubmitButton';

class Register extends React.Component {
    static propTypes = {
        loading: PropTypes.bool.isRequired,

        loadRegister: PropTypes.func.isRequired,
        setTitle: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            email: '',
            password: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.props.setTitle('Register new account');
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.loadRegister(
            this.state.firstname, this.state.lastname, this.state.email, this.state.password
        );
    }

    handleChange(event) {
        const {target} = event;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const {name} = target;

        this.setState({[name]: value});
    }

    render() {
        const {
            firstname, lastname, email, password
        } = this.state;
        return (
            <FormContainer title="Create new account" onSubmit={this.handleSubmit}>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="firstname"
                    label="First name"
                    name="firstname"
                    autoFocus
                    value={firstname}
                    onChange={this.handleChange}
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="lastname"
                    label="Last name"
                    name="lastname"
                    value={lastname}
                    onChange={this.handleChange}
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    value={email}
                    onChange={this.handleChange}
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    value={password}
                    onChange={this.handleChange}
                />
                <SubmitButton title="Register" loading={this.props.loading} />
            </FormContainer>
        );
    }
}

const mapStateToProps = (state) => ({
    loading: isRegisterLoading(state)
});

const actionCreators = {
    setTitle,
    loadRegister
};

export default connect(mapStateToProps, actionCreators)(Register);
