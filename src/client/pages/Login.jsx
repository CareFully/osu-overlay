/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {
    TextField, FormControlLabel, Grid, Link, Checkbox
} from '@material-ui/core';
import {Link as RouterLink} from 'react-router-dom';
import {isLoginLoading, getRememberMe} from '../selectors';
import {loadLogin, setRememberMe} from '../actions/auth';
import {setTitle} from '../actions/main';
import FormContainer from '../components/FormContainer';
import SubmitButton from '../components/SubmitButton';

class Login extends React.Component {
    static propTypes = {
        loading: PropTypes.bool.isRequired,
        rememberMe: PropTypes.bool.isRequired,

        loadLogin: PropTypes.func.isRequired,
        setRememberMe: PropTypes.func.isRequired,
        setTitle: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRememberMe = this.handleRememberMe.bind(this);
    }

    componentDidMount() {
        this.props.setTitle('Login');
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.loadLogin(this.state.email, this.state.password);
    }

    handleChange(event) {
        const {target} = event;
        const {name, value} = target;

        this.setState({[name]: value});
    }

    handleRememberMe(event) {
        this.props.setRememberMe(event.target.checked);
    }

    render() {
        const {email, password} = this.state;
        return (
            <FormContainer title="Log in" onSubmit={this.handleSubmit}>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                    value={email}
                    onChange={this.handleChange}
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    value={password}
                    onChange={this.handleChange}
                />
                <FormControlLabel
                    control={<Checkbox name="remember" value="remember" color="primary" checked={this.props.rememberMe} onChange={this.handleRememberMe} />}
                    label="Remember me"
                />
                <SubmitButton title="Sign In" loading={this.props.loading} />
                <Grid container>
                    <Grid item xs>
                        <Link component={RouterLink} to="/reset-password" variant="body2">
                            Forgot password?
                        </Link>
                    </Grid>
                    <Grid item>
                        <Link component={RouterLink} to="/register" variant="body2">
                            Dont have an account? Sign Up
                        </Link>
                    </Grid>
                </Grid>
            </FormContainer>
        );
    }
}

const mapStateToProps = (state) => ({
    loading: isLoginLoading(state),
    rememberMe: getRememberMe(state)
});

const actionCreators = {
    setTitle,
    loadLogin,
    setRememberMe
};

export default connect(mapStateToProps, actionCreators)(Login);
