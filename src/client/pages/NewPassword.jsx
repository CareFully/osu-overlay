/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {TextField} from '@material-ui/core';
import {isNewPasswordLoading, getQueryParams} from '../selectors';
import {loadNewPassword} from '../actions/auth';
import {setTitle} from '../actions/main';
import FormContainer from '../components/FormContainer';
import SubmitButton from '../components/SubmitButton';

class NewPassword extends React.Component {
    static propTypes = {
        loading: PropTypes.bool.isRequired,
        query: PropTypes.object.isRequired,

        loadNewPassword: PropTypes.func.isRequired,
        setTitle: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            password: '',
            confirmPassword: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.props.setTitle('Set a new password');
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.loadNewPassword(
            this.props.query.token, this.state.password, this.state.confirmPassword
        );
    }

    handleChange(event) {
        const {target} = event;
        const {name, value} = target;
        this.setState({[name]: value});
    }

    render() {
        const {password, confirmPassword} = this.state;
        return (
            <FormContainer title="Set new password" onSubmit={this.handleSubmit}>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="New password"
                    type="password"
                    id="password"
                    value={password}
                    onChange={this.handleChange}
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="confirmPassword"
                    label="Confirm new password"
                    type="password"
                    id="confirmPassword"
                    autoComplete="current-password"
                    value={confirmPassword}
                    onChange={this.handleChange}
                />
                <SubmitButton title="Set new password" loading={this.props.loading} />
            </FormContainer>
        );
    }
}

const mapStateToProps = (state) => ({
    loading: isNewPasswordLoading(state),
    query: getQueryParams(state)
});

const actionCreators = {
    setTitle,
    loadNewPassword
};

export default connect(mapStateToProps, actionCreators)(NewPassword);
