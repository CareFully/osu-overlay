/* eslint-disable class-methods-use-this */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {TextField} from 'mui-rff';
import {Form} from 'react-final-form';
import {isUpdateLoading} from '../selectors';
import {setTitle} from '../actions/main';
import {loadUpdate} from '../actions/user';
import FormContainer from '../components/FormContainer';
import withUser from '../hocs/withUser';
import SubmitButton from '../components/SubmitButton';

class Account extends React.Component {
    static propTypes = {
        loading: PropTypes.bool.isRequired,
        user: PropTypes.object,

        setTitle: PropTypes.func.isRequired,
        loadUpdate: PropTypes.func.isRequired
    };

    static defaultProps = {
        user: null
    }

    constructor(props) {
        super(props);
        this.state = {};

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.props.setTitle('Account settings');
    }

    handleSubmit(values) {
        console.log('handleSubmit', values);
        this.props.loadUpdate(this.props.user._id, values.firstname, values.lastname, values.email);
    }

    render() {
        const {loading, user} = this.props;
        return (
            <FormContainer title="Change account settings">
                <Form
                    onSubmit={this.handleSubmit}
                    initialValues={user}
                    render={({handleSubmit}) => (
                        <form onSubmit={handleSubmit} noValidate>
                            <TextField name="firstname" label="First name" />
                            <TextField name="lastname" label="Last name" />
                            <TextField name="email" label="Email" />
                            <SubmitButton title="Save" loading={loading} />
                        </form>
                    )}
                />
            </FormContainer>
        );
    }
}

const mapStateToProps = (state) => ({
    loading: isUpdateLoading(state)
});

const actionCreators = {
    setTitle,
    loadUpdate
};

export default withUser(connect(mapStateToProps, actionCreators)(Account));
