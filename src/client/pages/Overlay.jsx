/* eslint-disable class-methods-use-this */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {compose} from 'redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import {setTitle} from '../actions/main';
import SocketEvent from '../components/SocketEvent';
import {loadOverlays} from '../actions/overlay';
import StreamCompanion from '../components/overlay/StreamCompanion';
import {getOverlays} from '../selectors';
import Text from '../components/overlay/Text';
import Image from '../components/overlay/Image';
import Overlays from '../components/overlay/Overlays';
import Obs from '../components/overlay/Obs';
import WebFrame from '../components/overlay/WebFrame';

export const OVERLAY_COMPONENTS = {
    text: Text,
    image: Image,
    frame: WebFrame
};

class Overlay extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        setTitle: PropTypes.func.isRequired,
        loadOverlays: PropTypes.func.isRequired,
        overlays: PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
        };
        this.onOverlayUpdate = this.onOverlayUpdate.bind(this);
    }

    componentDidMount() {
        this.props.setTitle('Overlay');
        this.props.loadOverlays();
    }

    onOverlayUpdate() {
        console.log('Overlay update');
        this.props.loadOverlays();
    }

    render() {
        const {
            classes, overlays
        } = this.props;
        return (
            <div className={classes.root}>
                <CssBaseline />
                <StreamCompanion />
                <Obs />
                <SocketEvent event="overlay/update" handler={this.onOverlayUpdate} />
                <Overlays overlays={overlays} />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    overlays: getOverlays(state)
});

const actionCreators = {
    setTitle,
    loadOverlays
};

export default compose(
    withStyles({
        root: {
            width: '100vw',
            height: '100vh',
            position: 'relative',
            overflow: 'hidden'
        }
    }),
    connect(mapStateToProps, actionCreators)
)(Overlay);
