/* eslint-disable max-len */
/* eslint-disable class-methods-use-this */
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {
    Drawer,
    Toolbar,
    List,
    ListItem,
    ListItemText,
    Divider,
    withStyles,
    ListItemSecondaryAction,
    Switch,
    ListSubheader
} from '@material-ui/core';
import {compose} from 'redux';
import {setTitle} from '../actions/main';
import {
    loadOverlays,
    loadUpdateOverlay,
    toggleOverlayInGame,
    toggleOverlayInMenu,
    toggleOverlayOnDesktop,
    setOverlayOption,
    setOverlayHiddenOption
} from '../actions/overlay';
import {setActiveScene} from '../actions/obs';
import {DRAWER_WIDTH, IN_GAME_SCENE} from '../utils/constants';
import Overlays from '../components/overlay/Overlays';
import {
    getOverlays,
    getObsActiveScene,
    getOsuTokens
} from '../selectors';
import {OSU_STATUS_PLAYING, OSU_STATUS_LISTENING} from '../components/overlay/StreamCompanion';
import ListToggle from '../components/ListToggle';
import OverlayOptions from '../components/overlay/OverlayOptions';
import AddOverlayList from '../components/overlay/AddOverlayList';
import OverlaysListItem from '../components/overlay/OverlaysListItem';
import {
    setToken
} from '../actions/stream-companion';

class EditOverlay extends React.Component {
    static propTypes = {
        setTitle: PropTypes.func.isRequired,
        setToken: PropTypes.func.isRequired,
        toggleOverlayInGame: PropTypes.func.isRequired,
        toggleOverlayInMenu: PropTypes.func.isRequired,
        toggleOverlayOnDesktop: PropTypes.func.isRequired,
        setOverlayOption: PropTypes.func.isRequired,
        setOverlayHiddenOption: PropTypes.func.isRequired,
        setActiveScene: PropTypes.func.isRequired,
        loadOverlays: PropTypes.func.isRequired,
        loadUpdateOverlay: PropTypes.func.isRequired,

        classes: PropTypes.object.isRequired,

        overlays: PropTypes.array.isRequired,
        osuTokens: PropTypes.object.isRequired,
        obsActiveScene: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedOverlay: null
        };
        this.onOverlayClicked = this.onOverlayClicked.bind(this);
        this.onToggleInGame = this.onToggleInGame.bind(this);
        this.onToggleInGameScene = this.onToggleInGameScene.bind(this);
        this.onToggleOverlayInGame = this.onToggleOverlayInGame.bind(this);
        this.onToggleOverlayInMenu = this.onToggleOverlayInMenu.bind(this);
        this.onToggleOverlayOnDesktop = this.onToggleOverlayOnDesktop.bind(this);
        this.onSaveOverlay = this.onSaveOverlay.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.handleHiddenOptionChange = this.handleHiddenOptionChange.bind(this);
    }

    componentDidMount() {
        this.props.setTitle('Overlay Editor');
        this.props.loadOverlays();
    }

    onOverlayClicked(overlay) {
        this.setState({
            selectedOverlay: overlay.name
        });
    }

    onToggleInGame() {
        this.props.setToken(
            'status',
            this.props.osuTokens.status === OSU_STATUS_PLAYING
                ? OSU_STATUS_LISTENING
                : OSU_STATUS_PLAYING
        );
    }

    onToggleInGameScene() {
        this.props.setActiveScene(
            this.props.obsActiveScene === IN_GAME_SCENE
                ? 'Not In Game' : IN_GAME_SCENE
        );
    }

    onToggleOverlayInGame() {
        this.props.toggleOverlayInGame(this.getSelectedOverlayId());
    }

    onToggleOverlayInMenu() {
        this.props.toggleOverlayInMenu(this.getSelectedOverlayId());
    }

    onToggleOverlayOnDesktop() {
        this.props.toggleOverlayOnDesktop(this.getSelectedOverlayId());
    }

    handleOptionChange(name, value) {
        console.log('handleOptionChange', name, value);
        this.props.setOverlayOption(this.getSelectedOverlayId(), name, value);
    }

    handleHiddenOptionChange(name, value) {
        this.props.setOverlayHiddenOption(this.getSelectedOverlayId(), name, value);
    }

    onSaveOverlay() {
        this.props.loadUpdateOverlay(this.getSelectedOverlay());
    }

    getSelectedOverlay() {
        return this.props.overlays.find((item) => item.name === this.state.selectedOverlay);
    }

    getSelectedOverlayId() {
        return this.props.overlays.findIndex((item) => item.name === this.state.selectedOverlay);
    }

    render() {
        const {
            classes, overlays, osuTokens, obsActiveScene
        } = this.props;
        const overlay = this.getSelectedOverlay();

        return (
            <div className={classes.root}>
                <main className={classes.content}>
                    <Overlays selectedOverlayIndex={this.getSelectedOverlayId()} overlays={overlays} onOverlayClicked={this.onOverlayClicked} />
                </main>
                <Drawer
                    className={classes.drawer}
                    variant="permanent"
                    classes={{
                        paper: classes.drawerPaper
                    }}
                    anchor="right"
                >
                    <Toolbar />
                    <div className={classes.drawerContainer}>
                        <List dense>
                            <OverlaysListItem />
                        </List>
                        <AddOverlayList />
                        <Divider />
                        <List dense subheader={<ListSubheader disableSticky>Overlay states</ListSubheader>}>
                            <ListItem>
                                <ListItemText primary="In Game Scene" />
                                <ListItemSecondaryAction>
                                    <Switch
                                        edge="end"
                                        onChange={this.onToggleInGameScene}
                                        checked={obsActiveScene === IN_GAME_SCENE}
                                    />
                                </ListItemSecondaryAction>
                            </ListItem>
                            <ListItem>
                                <ListItemText primary="In Game osu!" />
                                <ListItemSecondaryAction>
                                    <Switch
                                        edge="end"
                                        onChange={this.onToggleInGame}
                                        checked={osuTokens.status === OSU_STATUS_PLAYING}
                                    />
                                </ListItemSecondaryAction>
                            </ListItem>
                        </List>
                        <Divider />
                        {overlay && (
                            <List
                                key={overlay.name}
                                dense
                                subheader={
                                    <ListSubheader disableSticky>{overlay.name}</ListSubheader>
                                }
                            >
                                <ListToggle label="Show in game" name="inGame" onChange={this.onToggleOverlayInGame} value={overlay.inGame} />
                                <ListToggle label="Show in menu" name="inMenu" onChange={this.onToggleOverlayInMenu} value={overlay.inMenu} />
                                <ListToggle label="Show on desktop" name="onDesktop" onChange={this.onToggleOverlayOnDesktop} value={overlay.onDesktop} />
                                <OverlayOptions title="Options" overlayType={overlay.type} options={overlay.options} onChange={this.handleOptionChange} />
                                <OverlayOptions title="Hidden Options" overlayType={overlay.type} options={overlay.hiddenOptions} onChange={this.handleHiddenOptionChange} disableable />
                                <ListItem button onClick={this.onSaveOverlay}>
                                    <ListItemText primary="Save" />
                                </ListItem>
                            </List>
                        )}

                    </div>
                </Drawer>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    overlays: getOverlays(state),
    osuTokens: getOsuTokens(state),
    obsActiveScene: getObsActiveScene(state)
});

const actionCreators = {
    setTitle,
    toggleOverlayInGame,
    toggleOverlayInMenu,
    toggleOverlayOnDesktop,
    setOverlayOption,
    setOverlayHiddenOption,
    setActiveScene,
    loadOverlays,
    loadUpdateOverlay,
    setToken
};

export default compose(
    withStyles(() => ({
        root: {
            display: 'flex',
            height: '100%'
        },
        drawer: {
            width: DRAWER_WIDTH * 1.25,
            flexShrink: 0
        },
        drawerPaper: {
            width: DRAWER_WIDTH * 1.25
        },
        drawerContainer: {
            overflow: 'auto'
        },
        content: {
            width: '100%',
            height: '100%'
        }
    })),
    connect(mapStateToProps, actionCreators)
)(EditOverlay);
