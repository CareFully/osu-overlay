/* eslint-disable prefer-destructuring */
import {error, success} from 'react-toastify-redux';
import {push} from 'connected-react-router';
import requestAction from '../utils/requestAction';

export const SET_USER = 'AUTH_SET_USER';
export const SET_TOKEN = 'AUTH_SET_TOKEN';
export const SET_REMEMBER_ME = 'AUTH_SET_REMEMBER_ME';
export const CLEAR_USER = 'AUTH_CLEAR_USER';
export const ON_RESET_PASSWORD = 'AUTH_ON_RESET_PASSWORD';
export const ON_NEW_PASSWORD = 'AUTH_ON_NEW_PASSWORD';

export const FETCH_LOGIN = 'AUTH_FETCH_LOGIN';
export const FETCH_RESET_PASSWORD = 'AUTH_FETCH_RESET_PASSWORD';
export const FETCH_NEW_PASSWORD = 'AUTH_FETCH_NEW_PASSWORD';
export const FETCH_TOKEN = 'AUTH_FETCH_TOKEN';

export function setRememberMe(remember) {
    return {type: SET_REMEMBER_ME, value: remember};
}

export function onLogin(response) {
    return (dispatch) => {
        dispatch({type: SET_USER, user: response.user});
        dispatch({type: SET_TOKEN, token: response.token});
        dispatch(push('/'));
        dispatch(success(`Welcome back, ${response.user.firstname}`));
    };
}

export function onLoginFailed() {
    return (dispatch) => {
        dispatch(error('Failed to log in'));
    };
}

export function loadLogin(email, password) {
    return (dispatch) => {
        dispatch(requestAction({
            url: '/api/auth/login',
            method: 'POST',
            data: {
                email: email,
                password: password
            },
            onSuccess: onLogin,
            onFailure: onLoginFailed,
            label: FETCH_LOGIN
        }));
    };
}

export function logout() {
    return (dispatch) => {
        dispatch(push('/'));
        dispatch({type: CLEAR_USER});
    };
}

function onResetPassword() {
    return (dispatch) => {
        dispatch(push('/'));
        dispatch(success('Please check your email to complete password reset'));
    };
}

function onResetPasswordFailed(response) {
    return (dispatch) => {
        dispatch(error(response.message));
    };
}

export function loadResetPassword(email) {
    return (dispatch) => {
        dispatch(requestAction({
            url: '/api/auth/reset-password',
            method: 'POST',
            data: {
                email: email
            },
            onSuccess: onResetPassword,
            onFailure: onResetPasswordFailed,
            label: FETCH_RESET_PASSWORD
        }));
    };
}

function onNewPassword() {
    return (dispatch) => {
        dispatch(push('/login'));
        dispatch(success('Your password has been updated. Please log in to continue'));
    };
}

function onNewPasswordFailed(response) {
    return (dispatch) => {
        dispatch(error(response.message));
    };
}

export function loadNewPassword(token, password, confirmPassword) {
    return (dispatch) => {
        dispatch(requestAction({
            url: '/api/auth/new-password',
            method: 'POST',
            data: {
                token: token,
                password: password,
                confirmPassword: confirmPassword
            },
            onSuccess: onNewPassword,
            onFailure: onNewPasswordFailed,
            label: FETCH_NEW_PASSWORD
        }));
    };
}

function onToken(response) {
    return (dispatch) => {
        dispatch({type: SET_USER, user: response.user});
        dispatch({type: SET_TOKEN, token: response.token});
    };
}

function onTokenFailed() {
    return {type: CLEAR_USER};
}

export function loadToken() {
    return (dispatch) => {
        dispatch(requestAction({
            url: '/api/auth/token',
            method: 'GET',
            onSuccess: onToken,
            onFailure: onTokenFailed,
            label: FETCH_TOKEN
        }));
    };
}
