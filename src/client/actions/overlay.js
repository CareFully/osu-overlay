/* eslint-disable no-use-before-define */
import {error, success} from 'react-toastify-redux';
import requestAction from '../utils/requestAction';

export const CREATE_OVERLAY = 'OVERLAY_CREATE_OVERLAY';
export const SET_OVERLAYS = 'OVERLAY_SET_OVERLAYS';
export const FETCH_OVERLAYS = 'OVERLAY_FETCH_OVERLAYS';
export const FETCH_CREATE_OVERLAY = 'OVERLAY_FETCH_CREATE_OVERLAY';
export const FETCH_UPDATE_OVERLAY = 'OVERLAY_FETCH_UPDATE_OVERLAY';
export const FETCH_DELETE_OVERLAY = 'OVERLAY_FETCH_DELETE_OVERLAY';
export const FETCH_SET_ORDER = 'OVERLAY_FETCH_SET_ORDER';

export const TOGGLE_OVERLAY_IN_GAME = 'OVERLAY_SET_OVERLAY_IN_GAME';
export const TOGGLE_OVERLAY_IN_MENU = 'OVERLAY_SET_OVERLAY_IN_MENU';
export const TOGGLE_OVERLAY_ON_DESKTOP = 'OVERLAY_SET_OVERLAY_ON_DESKTOP';
export const SET_OVERLAY_ORDER = 'OVERLAY_SET_OVERLAY_ORDER';
export const SET_OVERLAY_OPTION = 'OVERLAY_SET_OVERLAY_OPTION';
export const SET_OVERLAY_HIDDEN_OPTION = 'OVERLAY_SET_OVERLAY_HIDDEN_OPTION';
export const DELETE_OVERLAY = 'OVERLAY_DELETE_OVERLAY';

// Edit actions

export function createOverlay(overlay) {
    return (dispatch) => {
        dispatch(loadCreateOverlay(overlay));
    };
}

export function setOverlayOrder(index, order) {
    return (dispatch, getState) => {
        const overlay = getState().overlay.overlays[index];
        dispatch({type: SET_OVERLAY_ORDER, index, order});
        dispatch(loadSetOrder(overlay._id, order));
    };
}

export function toggleOverlayInGame(index) {
    return (dispatch, getState) => {
        dispatch({type: TOGGLE_OVERLAY_IN_GAME, index});
        const overlay = getState().overlay.overlays[index];
        dispatch(loadUpdateOverlay(overlay));
    };
}

export function toggleOverlayInMenu(index) {
    return (dispatch, getState) => {
        dispatch({type: TOGGLE_OVERLAY_IN_MENU, index});
        const overlay = getState().overlay.overlays[index];
        dispatch(loadUpdateOverlay(overlay));
    };
}

export function toggleOverlayOnDesktop(index) {
    return (dispatch, getState) => {
        dispatch({type: TOGGLE_OVERLAY_ON_DESKTOP, index});
        const overlay = getState().overlay.overlays[index];
        dispatch(loadUpdateOverlay(overlay));
    };
}

export function setOverlayOption(index, name, value) {
    return {
        type: SET_OVERLAY_OPTION, index, name, value
    };
}

export function setOverlayHiddenOption(index, name, value) {
    return {
        type: SET_OVERLAY_HIDDEN_OPTION, index, name, value
    };
}

export function deleteOverlay(index) {
    return (dispatch, getState) => {
        const overlay = getState().overlay.overlays[index];
        dispatch({type: DELETE_OVERLAY, index});
        dispatch(loadDeleteOverlay(overlay._id));
    };
}

function onOverlays(response) {
    return {type: SET_OVERLAYS, response};
}

function onOverlaysFailed(response) {
    return (dispatch) => {
        dispatch(error(`Loading overlays failed: ${response.message}`));
    };
}

function onUpdateOverlay() {
    return (dispatch) => {
        dispatch(success('Overlay saved'));
    };
}

function onUpdateOverlayFailed(response) {
    return (dispatch) => {
        dispatch(error(`Saving overlay failed: ${response.message}`));
    };
}

function onCreateOverlay() {
    return (dispatch) => {
        dispatch(success('New overlay created'));
        dispatch(loadOverlays());
    };
}

function onCreateOverlayFailed(response) {
    return (dispatch) => {
        dispatch(error(`Creating overlay failed: ${response.message}`));
    };
}

function onSetOrderFailed(response) {
    return (dispatch) => {
        dispatch(error(`Updating overlay order failed: ${response.message}`));
    };
}

function onDeleteOverlay() {
    return (dispatch) => {
        dispatch(success('Overlay deleted'));
    };
}

function onDeleteOverlayFailed(response) {
    return (dispatch) => {
        dispatch(error(`Deleting overlay failed: ${response.message}`));
    };
}

export function loadOverlays() {
    return (dispatch) => {
        dispatch(requestAction({
            url: '/api/overlay',
            onSuccess: onOverlays,
            onFailure: onOverlaysFailed,
            label: FETCH_OVERLAYS
        }));
    };
}

export function loadCreateOverlay(overlay) {
    return (dispatch) => {
        dispatch(requestAction({
            url: '/api/overlay',
            method: 'POST',
            data: overlay,
            onSuccess: onCreateOverlay,
            onFailure: onCreateOverlayFailed,
            label: FETCH_CREATE_OVERLAY
        }));
    };
}

export function loadUpdateOverlay(overlay) {
    return (dispatch) => {
        dispatch(requestAction({
            url: `/api/overlay/${overlay._id}`,
            method: 'PUT',
            data: overlay,
            onSuccess: onUpdateOverlay,
            onFailure: onUpdateOverlayFailed,
            label: FETCH_UPDATE_OVERLAY
        }));
    };
}

export function loadSetOrder(id, order) {
    return (dispatch) => {
        dispatch(requestAction({
            url: `/api/overlay/${id}/order`,
            method: 'PUT',
            data: {
                order: order
            },
            onFailure: onSetOrderFailed,
            label: FETCH_SET_ORDER
        }));
    };
}

export function loadDeleteOverlay(id) {
    return (dispatch) => {
        dispatch(requestAction({
            url: `/api/overlay/${id}`,
            method: 'DELETE',
            onSuccess: onDeleteOverlay,
            onFailure: onDeleteOverlayFailed,
            label: FETCH_DELETE_OVERLAY
        }));
    };
}
