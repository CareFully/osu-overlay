/* eslint-disable prefer-destructuring */
import {error} from 'react-toastify-redux';
import requestAction from '../utils/requestAction';

export const SET_TITLE = 'MAIN_SET_TITLE';
export const SET_FOO = 'MAIN_SET_FOO';

export const FETCH_FOO = 'MAIN_FETCH_FOO';

export function setTitle(title) {
    return {type: SET_TITLE, title: title};
}

// Sample request below

export function onFoo(response) {
    return {type: SET_FOO, response};
}

export function onFooFailed(response) {
    return (dispatch) => {
        dispatch(error(`Loading foo failed: ${response.message}`));
    };
}

export function loadFoo() {
    return (dispatch) => {
        dispatch(requestAction({
            url: '/api/foo',
            onSuccess: onFoo,
            onFailure: onFooFailed,
            label: FETCH_FOO
        }));
    };
}
