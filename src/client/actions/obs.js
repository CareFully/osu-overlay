export const SET_CONNECTION_STATUS = 'OBS_SET_CONNECTION_STATUS';
export const SET_ACTIVE_SCENE = 'OBS_SET_ACTIVE_SCENE';

export function setConnectionStatus(connectionStatus) {
    return {type: SET_CONNECTION_STATUS, connectionStatus};
}

export function setActiveScene(activeScene) {
    return {type: SET_ACTIVE_SCENE, activeScene};
}
