export const SET_CONNECTION_STATUS = 'SC_SET_CONNECTION_STATUS';
export const SET_TOKEN = 'SC_SET_TOKEN';

export function setConnectionStatus(connectionStatus) {
    return {type: SET_CONNECTION_STATUS, connectionStatus};
}

export function setToken(token, value) {
    return {type: SET_TOKEN, token, value};
}
