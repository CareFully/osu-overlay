import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import {toastsReducer} from 'react-toastify-redux';
import storage from 'redux-persist/lib/storage';
import sessionStorage from 'redux-persist/lib/storage/session';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import {persistReducer} from 'redux-persist';
import mainReducer from './main';
import streamCompanionReducer from './stream-companion';
import requestsReducer from './requests';
import authReducer from './auth';
import overlayReducer from './overlay';
import obsReducer from './obs';
import AuthTransform from '../transforms/auth';

const persistConfig = {
    key: 'root',
    stateReconciler: autoMergeLevel2,
    transforms: [AuthTransform],
    storage: storage,
    whitelist: ['auth']
};

const sessionPersistConfig = {
    key: 'auth',
    storage: sessionStorage,
    whitelist: ['user', 'token']
};

const createRootReducer = (history) => {
    const rootReducer = combineReducers({
        router: connectRouter(history),
        toasts: toastsReducer,
        requests: requestsReducer,
        auth: persistReducer(sessionPersistConfig, authReducer),
        main: mainReducer,
        streamCompanion: streamCompanionReducer,
        overlay: overlayReducer,
        obs: obsReducer
    });
    return persistReducer(persistConfig, rootReducer);
};

export default createRootReducer;
