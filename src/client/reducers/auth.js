import produce from 'immer';
import createReducer from '../utils/createReducer';
import {
    SET_USER, SET_TOKEN, SET_REMEMBER_ME, CLEAR_USER
} from '../actions/auth';

const initialState = {
    token: '',
    user: null,
    rememberMe: false
};

const setRememberMe = produce((draft, action) => {
    draft.rememberMe = action.value;
});

const setUser = produce((draft, action) => {
    draft.user = action.user;
});

const setToken = produce((draft, action) => {
    draft.token = action.token;
});

const clearUser = produce((draft) => {
    draft.token = '';
    draft.user = null;
});

export default createReducer(initialState, {
    [SET_USER]: setUser,
    [SET_TOKEN]: setToken,
    [CLEAR_USER]: clearUser,
    [SET_REMEMBER_ME]: setRememberMe
});
