import produce from 'immer';
import createReducer from '../utils/createReducer';
import {
    CREATE_OVERLAY,
    SET_OVERLAYS,
    SET_OVERLAY_OPTION,
    SET_OVERLAY_HIDDEN_OPTION,
    TOGGLE_OVERLAY_IN_GAME,
    TOGGLE_OVERLAY_IN_MENU,
    TOGGLE_OVERLAY_ON_DESKTOP,
    SET_OVERLAY_ORDER,
    DELETE_OVERLAY
} from '../actions/overlay';

const initialState = {
    overlays: []
};

const setOverlayOrder = produce((draft, action) => {
    const [item] = draft.overlays.splice(action.index, 1);
    draft.overlays.splice(action.order, 0, item);
});

const toggleOverlayInGame = produce((draft, action) => {
    draft.overlays[action.index].inGame = !draft.overlays[action.index].inGame;
});

const toggleOverlayInMenu = produce((draft, action) => {
    draft.overlays[action.index].inMenu = !draft.overlays[action.index].inMenu;
});

const toggleOverlayOnDesktop = produce((draft, action) => {
    draft.overlays[action.index].onDesktop = !draft.overlays[action.index].onDesktop;
});

const setOverlayOption = produce((draft, action) => {
    draft.overlays[action.index].options[action.name] = action.value;
});

const setOverlayHiddenOption = produce((draft, action) => {
    if (action.value === undefined) {
        delete draft.overlays[action.index].hiddenOptions[action.name];
    } else {
        draft.overlays[action.index].hiddenOptions[action.name] = action.value;
    }
});

const createOverlay = produce((draft, action) => {
    draft.overlays.push(action.overlay);
});

const setOverlays = produce((draft, action) => {
    draft.overlays = action.response;
});

const deleteOverlay = produce((draft, action) => {
    draft.overlays.splice(action.index, 1);
});

export default createReducer(initialState, {
    [CREATE_OVERLAY]: createOverlay,
    [SET_OVERLAYS]: setOverlays,
    [TOGGLE_OVERLAY_IN_GAME]: toggleOverlayInGame,
    [TOGGLE_OVERLAY_IN_MENU]: toggleOverlayInMenu,
    [TOGGLE_OVERLAY_ON_DESKTOP]: toggleOverlayOnDesktop,
    [SET_OVERLAY_OPTION]: setOverlayOption,
    [SET_OVERLAY_HIDDEN_OPTION]: setOverlayHiddenOption,
    [SET_OVERLAY_ORDER]: setOverlayOrder,
    [DELETE_OVERLAY]: deleteOverlay
});
