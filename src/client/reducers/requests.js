import produce from 'immer';
import createReducer from '../utils/createReducer';
import {REQUEST_START, REQUEST_END, REQUEST_ERROR} from '../actions/requests';

const initialState = {
    requests: {}
};

const requestStart = produce((draft, action) => {
    if (action.label) {
        draft[action.label] = {loading: true};
    } else {
        draft.global = {loading: true};
    }
});

const requestEnd = produce((draft, action) => {
    if (action.label) {
        draft[action.label].loading = false;
    } else {
        draft.global.loading = false;
    }
});

const requestError = produce((draft, action) => {
    if (action.label) {
        draft[action.label].error = action.error;
    } else {
        draft.global.loading = action.error;
    }
});

export default createReducer(initialState, {
    [REQUEST_START]: requestStart,
    [REQUEST_END]: requestEnd,
    [REQUEST_ERROR]: requestError
});
