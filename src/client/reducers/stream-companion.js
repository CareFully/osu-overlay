import produce from 'immer';
import createReducer from '../utils/createReducer';
import {
    SET_CONNECTION_STATUS,
    SET_TOKEN
} from '../actions/stream-companion';
import {CONNECTION_NOT_CONNECTED} from '../utils/constants';

const initialState = {
    connectionStatus: CONNECTION_NOT_CONNECTED,
    tokens: {}
};

const setConnectionStatus = produce((draft, action) => {
    draft.connectionStatus = action.connectionStatus;
});

const setToken = produce((draft, action) => {
    draft.tokens[action.token] = action.value;
});

export default createReducer(initialState, {
    [SET_CONNECTION_STATUS]: setConnectionStatus,
    [SET_TOKEN]: setToken
});
