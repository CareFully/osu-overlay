import produce from 'immer';
import createReducer from '../utils/createReducer';
import {CONNECTION_NOT_CONNECTED} from '../utils/constants';
import {SET_CONNECTION_STATUS, SET_ACTIVE_SCENE} from '../actions/obs';

const initialState = {
    connectionStatus: CONNECTION_NOT_CONNECTED,
    activeScene: ''
};

const setConnectionStatus = produce((draft, action) => {
    draft.connectionStatus = action.connectionStatus;
});

const setActiveScene = produce((draft, action) => {
    draft.activeScene = action.activeScene;
});

export default createReducer(initialState, {
    [SET_CONNECTION_STATUS]: setConnectionStatus,
    [SET_ACTIVE_SCENE]: setActiveScene
});
