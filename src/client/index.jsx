import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router';
import {PersistGate} from 'redux-persist/integration/react';
import {ThemeProvider} from '@material-ui/core/styles';
import initStore, {initPersistor, history} from './store/index';
import App from './components/App';
import Socket from './components/Socket';
import {theme} from './utils/themes';

const root = document.getElementById('app');
const store = initStore();
const persistor = initPersistor(store);

render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <ConnectedRouter history={history}>
                <ThemeProvider theme={theme}>
                    <Socket uri={process.env.APP_WSHOST} options={{transports: ['websocket']}}>
                        <App />
                    </Socket>
                </ThemeProvider>
            </ConnectedRouter>
        </PersistGate>
    </Provider>,
    root
);
