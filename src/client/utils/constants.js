/* eslint-disable object-curly-newline */

export const DRAWER_WIDTH = 240;

export const MENU_ITEMS = [
    {route: '/', name: 'Home', icon: 'home'},
    {route: '/overlay', name: 'Overlay', icon: 'tv'},
    {route: '/overlay/edit', name: 'Edit overlay', icon: 'edit'},
    {route: '/account', name: 'Account', icon: 'account_circle', protected: true}
];

export const CONNECTION_NOT_CONNECTED = 0;
export const CONNECTION_CONNECTING = 1;
export const CONNECTION_CONNECTED = 2;
export const IN_GAME_SCENE = 'In Game';
