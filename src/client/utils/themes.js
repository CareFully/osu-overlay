import {createMuiTheme} from '@material-ui/core/styles';
import {blue, pink} from '@material-ui/core/colors';

const baseTheme = createMuiTheme();
// eslint-disable-next-line import/prefer-default-export
export const theme = createMuiTheme({
    ...baseTheme,
    palette: {
        type: 'dark',
        primary: blue,
        secondary: pink
    },
    overrides: {

    }
});

export const overlayTheme = createMuiTheme({
    ...theme,
    typography: {
        fontFamily: "'Andika New Basic', sans-serif"
    }
});
