import React from 'react';
import {Route, Switch} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Home from '../pages/Home';
import Login from '../pages/Login';
import NotFound from '../pages/NotFound';
import 'react-toastify/dist/ReactToastify.css';
import '../assets/scss/style.scss';
import Register from '../pages/Register';
import ResetPassword from '../pages/ResetPassword';
import NewPassword from '../pages/NewPassword';
import withUser from '../hocs/withUser';
import {loadToken} from '../actions/auth';
import Account from '../pages/Account';
import UserRoute from './UserRoute';
import NormalRoute from './NormalRoute';
import Overlay from '../pages/Overlay';
import EditOverlay from '../pages/EditOverlay';

class App extends React.Component {
    static propTypes = {
        isLoggedIn: PropTypes.bool.isRequired,
        loadToken: PropTypes.func.isRequired
    };

    componentDidMount() {
        if (this.props.isLoggedIn) {
            this.props.loadToken();
        }
    }

    render() {
        return (
            <Switch>
                <NormalRoute path="/login" exact component={Login} />
                <NormalRoute path="/register" exact component={Register} />
                <NormalRoute path="/reset-password" exact component={ResetPassword} />
                <NormalRoute path="/new-password" exact component={NewPassword} />
                <NormalRoute path="/" exact component={Home} />
                <UserRoute path="/account" exact component={Account} />
                <NormalRoute path="/overlay/edit" exact component={EditOverlay} />
                <Route path="/overlay" component={Overlay} />
                <NormalRoute path="*" component={NotFound} />
            </Switch>
        );
    }
}

const actionCreators = {
    loadToken
};

export default withUser(connect(null, actionCreators)(App));
