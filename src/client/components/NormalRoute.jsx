/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import {Route} from 'react-router';
import {ToastContainer} from 'react-toastify-redux';
import {Slide} from 'react-toastify';
import CssBaseline from '@material-ui/core/CssBaseline';
import {withStyles} from '@material-ui/core';
import Header from './Header';
import {DRAWER_WIDTH} from '../utils/constants';

class NormalRoute extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,

        component: PropTypes.elementType.isRequired
    };

    render() {
        const {component: Component, classes, ...rest} = this.props;
        return (
            <Route
                {...rest}
                render={(props) => (
                    <div className={classes.root}>
                        <CssBaseline />
                        <Header />
                        <ToastContainer transition={Slide} className={classes.toastContainer} />
                        <div className={classes.content}>
                            <Component {...props} />
                        </div>
                    </div>
                )}
            />
        );
    }
}

export default withStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100vh'
    },
    toastContainer: {
        marginTop: '5em'
    },
    content: {
        [theme.breakpoints.up('md')]: {
            marginLeft: DRAWER_WIDTH
        },
        flexGrow: 1
    }
}))(NormalRoute);
