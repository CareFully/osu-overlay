import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {
    Box,
    Card,
    CardContent,
    Typography
} from '@material-ui/core';
import {CONNECTION_CONNECTED, CONNECTION_CONNECTING, CONNECTION_NOT_CONNECTED} from '../utils/constants';

class ConnectionStatus extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        connectionStatus: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    };

    render() {
        const {classes, name, connectionStatus} = this.props;
        let connectionClass;
        let connectionText;
        switch (connectionStatus) {
        case CONNECTION_CONNECTED:
            connectionClass = classes.connected;
            connectionText = 'Connected';
            break;
        case CONNECTION_CONNECTING:
            connectionClass = classes.connecting;
            connectionText = 'Connecting';
            break;
        case CONNECTION_NOT_CONNECTED:
        default:
            connectionClass = classes.notConnected;
            connectionText = 'No connection';
            break;
        }
        return (
            <Card className={classes.root}>
                <CardContent className={classes.cardContent}>
                    <Box display="flex" justifyContent="space-between" alignItems="center">
                        <Typography variant="body1">
                            {name}
                        </Typography>
                        <Typography variant="button" className={connectionClass}>
                            {connectionText}
                        </Typography>
                    </Box>
                </CardContent>
            </Card>
        );
    }
}

export default withStyles((theme) => ({
    root: {
        margin: theme.spacing(1, 0)
    },
    cardContent: {
        '&:last-child': {
            paddingBottom: 16
        }
    },
    connected: {
        color: theme.palette.success.main
    },
    connecting: {
        color: theme.palette.info.main
    },
    notConnected: {
        color: theme.palette.warning.main
    }
}))(ConnectionStatus);
