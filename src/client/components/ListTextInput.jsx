import React from 'react';
import {
    TextField
} from '@material-ui/core';
import PropTypes from 'prop-types';
import withDisableable from '../hocs/withDisableable';

class ListTextInput extends React.Component {
    static propTypes = {
        label: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
        disabled: PropTypes.bool
    };

    static defaultProps = {
        disabled: false
    }

    constructor(props) {
        super(props);
        this.onValueChanged = this.onValueChanged.bind(this);
    }

    onValueChanged(event) {
        const {target} = event;
        const {name, value} = target;
        this.props.onChange(name, value);
    }

    render() {
        const {
            label, name, value, disabled
        } = this.props;
        return (
            <TextField disabled={disabled} fullWidth type="text" label={label} size="small" variant="outlined" name={name} value={value} onChange={this.onValueChanged} />
        );
    }
}

export default withDisableable(ListTextInput);
