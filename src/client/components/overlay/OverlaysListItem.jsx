/* eslint-disable max-len */
/* eslint-disable no-shadow */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable class-methods-use-this */
import React from 'react';
import DragHandleIcon from '@material-ui/icons/DragHandle';
import {
    Box,
    Button,
    Card,
    CardContent,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    ListItem,
    ListItemText,
    Paper,
    Typography,
    withStyles
} from '@material-ui/core';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {DragDropContext, Draggable, Droppable} from 'react-beautiful-dnd';
import {compose} from 'redux';
import {
    createOverlay,
    setOverlayOrder,
    deleteOverlay
} from '../../actions/overlay';
import {getOverlays} from '../../selectors';

class OverlaysListItem extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        createOverlay: PropTypes.func.isRequired,
        setOverlayOrder: PropTypes.func.isRequired,
        deleteOverlay: PropTypes.func.isRequired,
        overlays: PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            createDialogOpen: false
        };
        this.onDragEnd = this.onDragEnd.bind(this);
        this.onOpenDialog = this.onOpenDialog.bind(this);
        this.onCloseDialog = this.onCloseDialog.bind(this);
        this.onDeleteOverlay = this.onDeleteOverlay.bind(this);
    }

    onOpenDialog() {
        this.setState({
            createDialogOpen: true
        });
    }

    onCloseDialog() {
        this.setState({
            createDialogOpen: false
        });
    }

    onDragEnd(data) {
        console.log('onDragEnd', data);
        if (data.destination) {
            this.props.setOverlayOrder(this.getOverlayId(data.draggableId), data.destination.index);
        }
    }

    onDeleteOverlay(index) {
        this.props.deleteOverlay(index);
    }

    getOverlayId(name) {
        return this.props.overlays.findIndex((item) => item.name === name);
    }

    render() {
        const {
            overlays, classes
        } = this.props;
        const {
            createDialogOpen
        } = this.state;
        return (
            <>
                <ListItem button onClick={() => this.onOpenDialog()}>
                    <ListItemText primary="Overlay list" />
                </ListItem>
                <Dialog open={createDialogOpen} onClose={this.onCloseDialog}>
                    <DialogTitle>Reorder, rename and remove overlays</DialogTitle>
                    <DialogContent>
                        <DragDropContext onDragEnd={this.onDragEnd}>
                            <Droppable droppableId="droppable">
                                {(provided, snapshot) => (
                                    <div ref={provided.innerRef} {...provided.droppableProps}>
                                        {overlays.map((item, index) => (
                                            <Draggable key={item.name} draggableId={item.name} index={index}>
                                                {(provided, snapshot) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                        className={classes.listItemContainer}
                                                    >
                                                        <Paper className={classes.listItem} elevation={3}>
                                                            <div className={classes.itemOptions}>
                                                                <DragHandleIcon />
                                                                <Typography className={classes.itemTitle} variant="body1">
                                                                    {item.name}
                                                                </Typography>
                                                            </div>
                                                            <Button onClick={() => this.onDeleteOverlay(index)} size="small" variant="contained" color="secondary">
                                                                Delete
                                                            </Button>
                                                        </Paper>
                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}

                                        {provided.placeholder}
                                    </div>
                                )}

                            </Droppable>
                        </DragDropContext>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.onCloseDialog} color="primary">
                            Close
                        </Button>
                    </DialogActions>
                </Dialog>
            </>
        );
    }
}
const mapStateToProps = (state) => ({
    overlays: getOverlays(state)
});

const actionCreators = {
    createOverlay,
    setOverlayOrder,
    deleteOverlay
};

export default compose(
    withStyles((theme) => ({
        listItem: {
            padding: theme.spacing(1),
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
        listItemContainer: {
            padding: theme.spacing(1, 0)
        },
        itemOptions: {
            display: 'flex',
            alignItems: 'center'
        },
        itemTitle: {
            marginLeft: theme.spacing(1)
        }
    })),
    connect(mapStateToProps, actionCreators)
)(OverlaysListItem);
