/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core';
import ImageComponent from 'material-ui-image';
import withPositioning from '../../hocs/withPositioning';

export const ANCHOR_TOP_LEFT = 0;
export const ANCHOR_TOP_RIGHT = 1;
export const ANCHOR_TOP_CENTER = 2;
export const ANCHOR_BOTTOM_LEFT = 3;
export const ANCHOR_BOTTOM_RIGHT = 4;
export const ANCHOR_BOTTOM_CENTER = 5;
export const ANCHOR_CENTER = 6;

class Image extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        url: PropTypes.string.isRequired,
        imageKey: PropTypes.string
    };

    static defaultProps = {
        imageKey: ''
    }

    render() {
        const {
            url, classes, imageKey
        } = this.props;
        return (
            <ImageComponent
                key={imageKey || url}
                animationDuration={1000}
                color="#00000000"
                disableSpinner
                className={classes.root}
                src={url}
                style={{paddingTop: 0, width: '100%', height: '100%'}}
                imageStyle={{
                    objectFit: 'cover',
                    width: '100%',
                    height: '100%',
                    bottom: '0px',
                    right: '0px'
                }}
            />
        );
    }
}

export default withPositioning(
    withStyles(() => ({
        root: {

        }
    }))(Image)
);
