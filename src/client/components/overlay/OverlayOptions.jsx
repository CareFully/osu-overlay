/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import {
    Collapse, List, ListItem, ListItemText, withStyles
} from '@material-ui/core';
import {ExpandLess, ExpandMore} from '@material-ui/icons';
import ListToggle from '../ListToggle';
import ListNumberInput from '../ListNumberInput';
import ListSelect from '../ListSelect';
import {ANCHORS, ANCHOR_TOP_LEFT} from '../../hocs/withPositioning';
import ListTextInput from '../ListTextInput';
import {OVERLAY_COMPONENTS} from '../../pages/Overlay';
import ListColorPicker from '../ListColorPicker';
import {TEXT_ALIGN_LEFT, TEXT_ALIGN} from './Text';

const OPTION_TYPE_NUMBER = 0;
const OPTION_TYPE_STRING = 1;
const OPTION_TYPE_SELECT = 2;
const OPTION_TYPE_TOGGLE = 3;
const OPTION_TYPE_COLOR = 4;

const COMPONENTS = {
    [OPTION_TYPE_NUMBER]: ListNumberInput,
    [OPTION_TYPE_STRING]: ListTextInput,
    [OPTION_TYPE_SELECT]: ListSelect,
    [OPTION_TYPE_TOGGLE]: ListToggle,
    [OPTION_TYPE_COLOR]: ListColorPicker
};

const UNIT_OPTIONS = ['px', '%'].map((unit) => ({name: unit, value: unit}));
const ANCHOR_OPTIONS = Object.entries(ANCHORS).map(([name, value]) => ({name, value}));
const TEXT_ALIGN_OPTIONS = Object.entries(TEXT_ALIGN).map(([name, value]) => ({name, value}));

const BASE_OPTIONS = [
    {
        name: 'show', type: OPTION_TYPE_TOGGLE, label: 'Visible', defaultValue: false
    },
    {
        name: 'x', type: OPTION_TYPE_NUMBER, label: 'X', defaultValue: 0
    },
    {
        name: 'y', type: OPTION_TYPE_NUMBER, label: 'Y', defaultValue: 0
    },
    {
        name: 'width', type: OPTION_TYPE_NUMBER, label: 'Width', defaultValue: 0
    },
    {
        name: 'height', type: OPTION_TYPE_NUMBER, label: 'Height', defaultValue: 0
    },
    {
        name: 'widthUnit', type: OPTION_TYPE_SELECT, label: 'Width Unit', defaultValue: 'px', options: UNIT_OPTIONS
    },
    {
        name: 'heightUnit', type: OPTION_TYPE_SELECT, label: 'Height Unit', defaultValue: 'px', options: UNIT_OPTIONS
    },
    {
        name: 'angle', type: OPTION_TYPE_NUMBER, label: 'Angle', defaultValue: 0
    },
    {
        name: 'anchor', type: OPTION_TYPE_SELECT, label: 'Anchor', defaultValue: ANCHOR_TOP_LEFT, options: ANCHOR_OPTIONS
    }
];

const EXTRA_OPTIONS = {
    text: [
        {
            name: 'text', type: OPTION_TYPE_STRING, label: 'Text', defaultValue: ''
        },
        {
            name: 'background', type: OPTION_TYPE_COLOR, label: 'Background Color', defaultValue: '#00000000'
        },
        {
            name: 'elevation', type: OPTION_TYPE_NUMBER, label: 'Elevation', defaultValue: 0
        },
        {
            name: 'border', type: OPTION_TYPE_NUMBER, label: 'Border', defaultValue: 0
        },
        {
            name: 'textSize', type: OPTION_TYPE_NUMBER, label: 'Text Size', defaultValue: 16
        },
        {
            name: 'textAlign', type: OPTION_TYPE_SELECT, label: 'Text Align', defaultValue: TEXT_ALIGN_LEFT, options: TEXT_ALIGN_OPTIONS
        },
        {
            name: 'textColor', type: OPTION_TYPE_COLOR, label: 'Text Color', defaultValue: '#ffffff'
        },
        {
            name: 'textElevation', type: OPTION_TYPE_NUMBER, label: 'Text Elevation', defaultValue: 0
        }
    ],
    image: [
        {
            name: 'url', type: OPTION_TYPE_STRING, label: 'URL', defaultValue: ''
        },
        {
            name: 'imageKey', type: OPTION_TYPE_STRING, label: 'Image Key', defaultValue: ''
        }
    ],
    frame: [
        {
            name: 'url', type: OPTION_TYPE_STRING, label: 'URL', defaultValue: ''
        }
    ]
};

class OverlayOptions extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        title: PropTypes.string.isRequired,
        overlayType: PropTypes.oneOf(Object.keys(OVERLAY_COMPONENTS)).isRequired,
        options: PropTypes.object.isRequired,
        onChange: PropTypes.func.isRequired,
        disableable: PropTypes.bool
    };

    static defaultProps = {
        disableable: false
    }

    constructor(props) {
        super(props);
        this.state = {
            open: false
        };
        this.onToggleCollapse = this.onToggleCollapse.bind(this);
    }

    onToggleCollapse() {
        this.setState((state) => ({
            open: !state.open
        }));
    }

    getComponentFromConfig(config) {
        const {
            options, onChange, disableable
        } = this.props;
        const Component = COMPONENTS[config.type];
        return (
            <Component
                {...config}
                key={config.name}
                value={options[config.name]}
                onChange={onChange}
                disableable={disableable}
            />
        );
    }

    render() {
        const {
            classes, title, overlayType
        } = this.props;
        const {open} = this.state;
        return (
            <>
                <ListItem button onClick={this.onToggleCollapse}>
                    <ListItemText primary={title} />
                    {open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <List className={classes.nested} component="div" disablePadding dense>
                        {BASE_OPTIONS.map((config) => this.getComponentFromConfig(config))}
                        {EXTRA_OPTIONS[overlayType] && EXTRA_OPTIONS[overlayType].map(
                            (config) => this.getComponentFromConfig(config)
                        )}
                    </List>
                </Collapse>
            </>
        );
    }
}

export default withStyles((theme) => ({
    nested: {
        paddingLeft: theme.spacing(2)
    }
}))(OverlayOptions);
