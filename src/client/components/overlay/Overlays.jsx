/* eslint-disable class-methods-use-this */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withStyles, ThemeProvider} from '@material-ui/core/styles';
import {compose} from 'redux';
import memoizeOne from 'memoize-one';
import {dequal} from 'dequal';
import memoizerific from 'memoizerific';
import {OSU_STATUS_PLAYING, OSU_STATUS_WATCHING} from './StreamCompanion';
import {getObsActiveScene, getOsuToken, getOsuTokens} from '../../selectors';
import Text from './Text';
import Image from './Image';
import {IN_GAME_SCENE} from '../../utils/constants';
import {overlayTheme} from '../../utils/themes';
import WebFrame from './WebFrame';

export const OVERLAY_COMPONENTS = {
    text: Text,
    image: Image,
    frame: WebFrame
};

/* const VIEW_STATE_IN_GAME = 0;
const VIEW_STATE_IN_MENU = 1;
const VIEW_STATE_ON_DESKTOP = 2; */

const modifiers = {
    round: (value) => Math.round(value),
    twoDecimals: (value) => Math.round(value * 100) / 100
};

function getPlaceholders(text) {
    const placeholders = [];
    const result = text.replace(/{([\w-]+(?::?[\w-]+)?)}/g, (substring, param) => {
        const parts = param.split(':', 2);
        let modifier = null;
        let placeholder = parts[0];
        if (parts.length > 1) {
            [modifier, placeholder] = parts;
        }
        if (modifier) {
            if (!modifiers[modifier]) {
                console.warn(`Modifier "${modifier}" does not exist. [placeholder "${placeholder}"]`);
                modifier = null;
            }
        }
        placeholders.push([placeholder, modifier]);
        return '${}';
    });

    const parts = result.split('${}');
    return [parts, placeholders];
}
const memoizedGetPlaceholders = memoizerific(50000)(getPlaceholders);

function replacePlaceholders(text, replaceable) {
    let result = '';
    const [parts, placeholders] = memoizedGetPlaceholders(text);
    for (let i = 0; i < parts.length; i++) {
        const part = parts[i];
        result += part;
        if (i < parts.length - 1) {
            const [placeholder, modifier] = placeholders[i];
            const value = replaceable[placeholder];
            if (typeof value !== 'undefined') {
                if (modifier) {
                    result += modifiers[modifier](value);
                } else {
                    result += value;
                }
            }
        }
    }
    return result;
}
const memoizedReplace = memoizeOne(replacePlaceholders, dequal);

class Overlays extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        overlays: PropTypes.array.isRequired,
        onOverlayClicked: PropTypes.func,
        selectedOverlayIndex: PropTypes.number,

        obsActiveScene: PropTypes.string.isRequired,

        osuStatus: PropTypes.number,
        osuTokens: PropTypes.object.isRequired
    };

    static defaultProps = {
        onOverlayClicked: undefined,
        selectedOverlayIndex: undefined,
        osuStatus: 0
    }

    replacePlaceholders(text) {
        const {
            osuTokens
        } = this.props;
        return memoizedReplace(text, osuTokens);
    }

    shouldShowOverlay(overlay) {
        const {
            osuStatus,
            obsActiveScene
        } = this.props;
        if (obsActiveScene !== IN_GAME_SCENE) {
            return overlay.onDesktop;
        }
        if (osuStatus === OSU_STATUS_PLAYING || osuStatus === OSU_STATUS_WATCHING) {
            return overlay.inGame;
        }
        return overlay.inMenu;
    }

    render() {
        const {
            classes, overlays, onOverlayClicked, selectedOverlayIndex
        } = this.props;
        return (
            <div className={classes.root}>
                <ThemeProvider theme={overlayTheme}>
                    {overlays.map((overlay, index) => {
                        const Component = OVERLAY_COMPONENTS[overlay.type];
                        return overlay.active && (
                            <Component
                                key={overlay.name}
                                {...overlay.options}
                                {...Object.entries(overlay.options).reduce((acc, [key, value]) => {
                                    if (typeof value === 'string') {
                                        return {...acc, [key]: this.replacePlaceholders(value)};
                                    }
                                    return {...acc, [key]: value};
                                }, {})}
                                {...(this.shouldShowOverlay(overlay) ? {} : overlay.hiddenOptions)}
                                onClick={() => onOverlayClicked && onOverlayClicked(overlay)}
                                editing={selectedOverlayIndex === index}
                            />
                        );
                    })}
                </ThemeProvider>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    obsActiveScene: getObsActiveScene(state),
    osuStatus: getOsuToken(state, 'status'),
    osuTokens: getOsuTokens(state)
});

const actionCreators = {};

export default compose(
    withStyles({
        root: {
            width: '100%',
            height: '100%',
            position: 'relative',
            overflow: 'hidden'
        }
    }),
    connect(mapStateToProps, actionCreators)
)(Overlays);
