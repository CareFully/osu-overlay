import React from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    List,
    ListItem,
    ListItemText,
    TextField
} from '@material-ui/core';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {
    createOverlay
} from '../../actions/overlay';
import {OVERLAY_COMPONENTS} from './Overlays';
import {getOverlays} from '../../selectors';

class AddOverlayList extends React.Component {
    static propTypes = {
        createOverlay: PropTypes.func.isRequired,
        overlays: PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            createDialogOpen: false,
            createType: 'text',
            createName: '',
            error: ''
        };
        this.onCreateOverlay = this.onCreateOverlay.bind(this);
        this.onOpenCreateDialog = this.onOpenCreateDialog.bind(this);
        this.onCloseCreateDialog = this.onCloseCreateDialog.bind(this);
        this.handleChangeCreateName = this.handleChangeCreateName.bind(this);
    }

    onOpenCreateDialog(type) {
        this.setState({
            createDialogOpen: true,
            createType: type,
            createName: '',
            error: ''
        });
    }

    onCloseCreateDialog() {
        this.setState({
            createDialogOpen: false
        });
    }

    handleChangeCreateName(event) {
        this.setState({
            createName: event.target.value
        });
    }

    onCreateOverlay() {
        const duplicate = this.props.overlays.find(
            (overlay) => overlay.name === this.state.createName
        );
        if (duplicate) {
            this.setState({
                error: 'Overlay with the specified name already exists.'
            });
            return;
        }
        this.setState({
            createDialogOpen: false
        });
        this.props.createOverlay({
            type: this.state.createType,
            name: this.state.createName
        });
    }

    render() {
        const {
            createDialogOpen, createType, createName, error
        } = this.state;
        return (
            <>
                <List dense>
                    {Object.keys(OVERLAY_COMPONENTS).map((key) => (
                        <ListItem button key={key} onClick={() => this.onOpenCreateDialog(key)}>
                            <ListItemText primary={`Add ${key}`} />
                        </ListItem>
                    ))}
                </List>
                <Dialog open={createDialogOpen} onClose={this.onCloseCreateDialog} aria-labelledby="form-dialog-title">
                    <DialogTitle>{`Create new ${createType} overlay`}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please enter a unique name for the overlay:
                        </DialogContentText>
                        <TextField
                            autoFocus
                            label="Name"
                            error={!!error}
                            helperText={error}
                            value={createName}
                            onChange={this.handleChangeCreateName}
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.onCloseCreateDialog} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.onCreateOverlay} color="primary">
                            Create
                        </Button>
                    </DialogActions>
                </Dialog>
            </>
        );
    }
}
const mapStateToProps = (state) => ({
    overlays: getOverlays(state)
});

const actionCreators = {
    createOverlay
};

export default connect(mapStateToProps, actionCreators)(AddOverlayList);
