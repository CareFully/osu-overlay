/* eslint-disable jsx-a11y/iframe-has-title */
import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core';
import withPositioning from '../../hocs/withPositioning';

class WebFrame extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        url: PropTypes.string.isRequired
    };

    render() {
        const {
            url, classes
        } = this.props;

        return (
            <iframe className={classes.root} src={url} />
        );
    }
}

export default withPositioning(
    withStyles(() => ({
        root: {
            width: '100%',
            height: '100%',
            overflow: 'hidden',
            zoom: '0.5'
        }
    }))(WebFrame)
);
