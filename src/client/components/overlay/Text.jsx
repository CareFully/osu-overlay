/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import {Typography, withStyles} from '@material-ui/core';
import {Spring} from 'react-spring/renderprops';
import withPositioning from '../../hocs/withPositioning';

export const TEXT_ALIGN_LEFT = 0;
export const TEXT_ALIGN_CENTER = 1;
export const TEXT_ALIGN_RIGHT = 2;
export const TEXT_ALIGN = {
    Left: TEXT_ALIGN_LEFT,
    Center: TEXT_ALIGN_CENTER,
    Right: TEXT_ALIGN_RIGHT
};
export const TEXT_ALIGN_CSS = {
    [TEXT_ALIGN_LEFT]: 'left',
    [TEXT_ALIGN_CENTER]: 'center',
    [TEXT_ALIGN_RIGHT]: 'right'
};

class Text extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        text: PropTypes.string.isRequired,
        background: PropTypes.string,
        elevation: PropTypes.number,
        border: PropTypes.number,
        textSize: PropTypes.number,
        textAlign: PropTypes.oneOf([
            TEXT_ALIGN_LEFT,
            TEXT_ALIGN_CENTER,
            TEXT_ALIGN_RIGHT
        ]),
        textColor: PropTypes.string,
        textElevation: PropTypes.number
    };

    static defaultProps = {
        background: '#00000000',
        elevation: 0,
        border: 0,
        textSize: 16,
        textAlign: TEXT_ALIGN_LEFT,
        textColor: '#ffffff',
        textElevation: 0
    }

    render() {
        const {
            text,
            classes,
            background,
            elevation,
            border,
            textElevation,
            textColor,
            textAlign,
            textSize
        } = this.props;
        return (
            <Spring
                to={{
                    backgroundColor: background,
                    borderWidth: `${border}px`,
                    boxShadow: `0px 0px ${elevation}px ${elevation / 2}px rgba(0,0,0,0.5)`,
                    textShadow: `${textElevation}px ${textElevation}px ${textElevation / 2}px rgba(0,0,0,0.5)`,
                    color: textColor,
                    textAlign: TEXT_ALIGN_CSS[textAlign],
                    fontSize: `${textSize}pt`
                }}
            >
                {(props) => (
                    <Typography className={classes.root} style={props} variant="body1">
                        {text}
                    </Typography>
                )}
            </Spring>
        );
    }
}

export default withPositioning(
    withStyles((/* theme */) => ({
        root: {
            width: '100%',
            height: '100%',
            borderStyle: 'solid',
            borderColor: 'black'
        }
    }))(Text)
);
