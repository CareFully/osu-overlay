/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Websocket from 'react-websocket';
import {
    setConnectionStatus,
    setToken
} from '../../actions/stream-companion';
import {
    CONNECTION_CONNECTED,
    CONNECTION_CONNECTING,
    CONNECTION_NOT_CONNECTED
} from '../../utils/constants';

export const OSU_STATUS_LISTENING = 1;
export const OSU_STATUS_PLAYING = 2;
export const OSU_STATUS_WATCHING = 8;
export const OSU_STATUS_RESULTS_SCREEN = 32;

class StreamCompanion extends React.Component {
    static propTypes = {
        setConnectionStatus: PropTypes.func.isRequired,
        setToken: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.socketRef = React.createRef();

        this.onMessage = this.onMessage.bind(this);
        this.onConnected = this.onConnected.bind(this);
        this.onDisconnected = this.onDisconnected.bind(this);
    }

    componentDidMount() {
        this.props.setConnectionStatus(CONNECTION_CONNECTING);
    }

    componentWillUnmount() {
        this.props.setConnectionStatus(CONNECTION_NOT_CONNECTED);
    }

    onConnected() {
        this.props.setConnectionStatus(CONNECTION_CONNECTED);
        this.socketRef.current.sendMessage(JSON.stringify([
            'status',
            'c300',
            'c100',
            'c50',
            'miss',
            'grade',
            'mapPosition',
            'time',
            'timeLeft',
            'unstableRate',
            'ppIfMapEndsNow',
            'ppIfRestFced',
            'simulatedPp',
            'mapArtistTitle',
            'mapDiff',
            'creator',
            'skin',
            'mods',
            'mAR',
            'mCS',
            'mOD',
            'mHP',
            'mStars',
            'mBpm',
            'backgroundImageLocation'
        ]));
    }

    onDisconnected() {
        this.props.setConnectionStatus(CONNECTION_NOT_CONNECTED);
    }

    onMessage(data) {
        let message = {};
        if (data) {
            message = JSON.parse(data);
            Object.entries(message).forEach(([token, value]) => {
                this.props.setToken(token, value);
            });
        }
    }

    render() {
        return (
            <Websocket
                url="ws://localhost:20727/tokens/"
                onMessage={this.onMessage}
                onOpen={this.onConnected}
                onClose={this.onDisconnected}
                ref={this.socketRef}
            />
        );
    }
}

const actionCreators = {
    setConnectionStatus,
    setToken
};

export default connect(null, actionCreators)(StreamCompanion);
