import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Websocket from 'react-websocket';
import {
    setConnectionStatus,
    setActiveScene
} from '../../actions/obs';
import {
    CONNECTION_CONNECTED,
    CONNECTION_CONNECTING,
    CONNECTION_NOT_CONNECTED
} from '../../utils/constants';

export const OBS_REQUEST_GET_CURRENT_SCENE = '1337';

class Obs extends React.Component {
    static propTypes = {
        setConnectionStatus: PropTypes.func.isRequired,
        setActiveScene: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.socketRef = React.createRef();

        this.onMessage = this.onMessage.bind(this);
        this.onConnected = this.onConnected.bind(this);
        this.onDisconnected = this.onDisconnected.bind(this);
    }

    componentDidMount() {
        this.props.setConnectionStatus(CONNECTION_CONNECTING);
    }

    componentWillUnmount() {
        this.props.setConnectionStatus(CONNECTION_NOT_CONNECTED);
    }

    onConnected() {
        this.props.setConnectionStatus(CONNECTION_CONNECTED);
        this.socketRef.current.sendMessage(JSON.stringify({
            'request-type': 'GetCurrentScene',
            'message-id': OBS_REQUEST_GET_CURRENT_SCENE
        }));
    }

    onDisconnected() {
        this.props.setConnectionStatus(CONNECTION_NOT_CONNECTED);
    }

    onMessage(data) {
        if (data) {
            const message = JSON.parse(data);
            if (message['message-id'] !== undefined && message.status === 'ok') {
                if (message['message-id'] === OBS_REQUEST_GET_CURRENT_SCENE) {
                    this.props.setActiveScene(message.name);
                }
            } else if (message['update-type'] === 'SwitchScenes') {
                this.props.setActiveScene(message['scene-name']);
            }
        }
    }

    render() {
        return (
            <Websocket
                url="ws://localhost:4444"
                onMessage={this.onMessage}
                onOpen={this.onConnected}
                onClose={this.onDisconnected}
                ref={this.socketRef}
            />
        );
    }
}

const actionCreators = {
    setConnectionStatus,
    setActiveScene
};

export default connect(null, actionCreators)(Obs);
