/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import SocketIO from 'socket.io-client';

export const SocketContext = React.createContext('socket');

class Socket extends React.Component {
    static propTypes = {
        children: PropTypes.node.isRequired,
        options: PropTypes.object,
        uri: PropTypes.string.isRequired
    };

    static defaultProps = {
        options: {}
    }

    constructor(props) {
        super(props);

        this.socket = SocketIO(props.uri, props.options);

        this.socket.status = 'initialized';

        this.socket.on('connect', () => {
            this.socket.status = 'connected';
            console.log('connected');
        });

        this.socket.on('disconnect', () => {
            this.socket.status = 'disconnected';
            console.log('disconnect');
        });

        this.socket.on('error', (err) => {
            this.socket.status = 'failed';
            console.error('error', err);
        });

        this.socket.on('reconnect', (data) => {
            this.socket.status = 'connected';
            console.log('reconnect', data);
        });

        this.socket.on('reconnect_attempt', () => {
            console.log('reconnect_attempt');
        });

        this.socket.on('reconnecting', () => {
            this.socket.status = 'reconnecting';
            console.log('reconnecting');
        });

        this.socket.on('reconnect_failed', (error) => {
            this.socket.status = 'failed';
            console.warn('reconnect_failed', error);
        });
    }

    render() {
        const {children} = this.props;
        return (
            <SocketContext.Provider value={this.socket}>
                {children}
            </SocketContext.Provider>
        );
    }
}

export default Socket;
