import React from 'react';
import {
    MenuItem, TextField
} from '@material-ui/core';
import PropTypes from 'prop-types';
import withDisableable from '../hocs/withDisableable';

class ListSelect extends React.Component {
    static propTypes = {
        label: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
        onChange: PropTypes.func.isRequired,
        options: PropTypes.array.isRequired,
        disabled: PropTypes.bool
    };

    static defaultProps = {
        disabled: false
    }

    constructor(props) {
        super(props);
        this.onValueChanged = this.onValueChanged.bind(this);
    }

    onValueChanged(event) {
        const {target} = event;
        const {name, value} = target;
        this.props.onChange(name, value);
    }

    render() {
        const {
            label, name, value, options, disabled
        } = this.props;
        return (
            <TextField fullWidth select disabled={disabled} label={label} size="small" variant="outlined" name={name} value={value} onChange={this.onValueChanged}>
                {options.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                        {option.name}
                    </MenuItem>
                ))}
            </TextField>
        );
    }
}

export default withDisableable(ListSelect);
