import React from 'react';
import {
    ListItemSecondaryAction, ListItemText, Switch
} from '@material-ui/core';
import PropTypes from 'prop-types';
import withDisableable from '../hocs/withDisableable';

class ListToggle extends React.Component {
    static propTypes = {
        label: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        value: PropTypes.bool.isRequired,
        onChange: PropTypes.func.isRequired,
        disabled: PropTypes.bool
    };

    static defaultProps = {
        disabled: false
    }

    constructor(props) {
        super(props);
        this.onValueChanged = this.onValueChanged.bind(this);
    }

    onValueChanged(event) {
        const {target} = event;
        const {name, checked} = target;
        this.props.onChange(name, checked);
    }

    render() {
        const {
            label, name, value, disabled
        } = this.props;
        return (
            <>
                <ListItemText primary={label} />
                <ListItemSecondaryAction>
                    <Switch disabled={disabled} name={name} edge="end" onChange={this.onValueChanged} checked={value} />
                </ListItemSecondaryAction>
            </>
        );
    }
}

export default withDisableable(ListToggle);
