import React from 'react';
import {
    AppBar, Toolbar, IconButton, Typography, Button
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import {withStyles} from '@material-ui/core/styles';
import {Link as RouterLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import {compose} from 'redux';
import {connect} from 'react-redux';
import withUser from '../hocs/withUser';
import {logout} from '../actions/auth';
import {getTitle} from '../selectors';
import {DRAWER_WIDTH} from '../utils/constants';
import AppDrawer from './AppDrawer';

class Header extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        isLoggedIn: PropTypes.bool.isRequired,
        title: PropTypes.string.isRequired,

        logout: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            menuOpen: false
        };

        this.onLogout = this.onLogout.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
    }

    toggleMenu() {
        this.setState((state) => ({
            menuOpen: !state.menuOpen
        }));
    }

    onLogout() {
        this.props.logout();
    }

    render() {
        const {classes, title} = this.props;
        return (
            <>
                <AppBar position="sticky" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            className={classes.menuButton}
                            edge="start"
                            color="inherit"
                            aria-label="open drawer"
                            onClick={this.toggleMenu}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title} noWrap>
                            {title}
                        </Typography>
                        {this.props.isLoggedIn
                            ? <Button color="inherit" onClick={this.onLogout}>Log out</Button>
                            : <Button color="inherit" component={RouterLink} to="/login">Login</Button>}
                    </Toolbar>
                </AppBar>
                <AppDrawer open={this.state.menuOpen} onClose={this.toggleMenu} />
            </>
        );
    }
}

const mapStateToProps = (state) => ({
    title: getTitle(state)
});

const actionCreators = {
    logout
};

export default withUser(compose(
    withStyles((theme) => ({
        title: {
            flexGrow: 1
        },
        appBar: {
            zIndex: theme.zIndex.drawer + 1,
            [theme.breakpoints.up('md')]: {
                width: `calc(100% - ${DRAWER_WIDTH}px)`,
                marginLeft: DRAWER_WIDTH
            }
        },
        menuButton: {
            marginRight: theme.spacing(2),
            [theme.breakpoints.up('md')]: {
                display: 'none'
            }
        }
    })),
    connect(mapStateToProps, actionCreators)
)(Header));
