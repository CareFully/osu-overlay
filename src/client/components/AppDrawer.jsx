import React from 'react';
import PropTypes from 'prop-types';
import {
    Drawer,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Divider,
    withWidth,
    Icon,
    Typography,
    Toolbar
} from '@material-ui/core';
import {withStyles, withTheme} from '@material-ui/core/styles';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {Link as RouterLink} from 'react-router-dom';
import withUser from '../hocs/withUser';
import {getTitle, getPathname} from '../selectors';
import {DRAWER_WIDTH, MENU_ITEMS} from '../utils/constants';

class AppDrawer extends React.Component {
    static propTypes = {
        classes: PropTypes.object.isRequired,
        width: PropTypes.oneOf(['lg', 'md', 'sm', 'xl', 'xs']).isRequired,
        onClose: PropTypes.func.isRequired,
        open: PropTypes.bool.isRequired,
        pathname: PropTypes.string.isRequired,
        isLoggedIn: PropTypes.bool.isRequired
    };

    render() {
        const {
            classes, width, open, onClose, isLoggedIn, pathname
        } = this.props;

        const list = (
            <div>
                <Toolbar>
                    <Typography variant="h6" noWrap>
                        {process.env.APP_NAME}
                    </Typography>
                </Toolbar>
                <Divider />
                <List>
                    {MENU_ITEMS.map((item) => {
                        if (item.protected && !isLoggedIn) {
                            return null;
                        }
                        return (
                            <ListItem
                                button
                                key={item.name}
                                component={RouterLink}
                                to={item.route}
                                selected={pathname === item.route}
                            >
                                <ListItemIcon><Icon>{item.icon}</Icon></ListItemIcon>
                                <ListItemText primary={item.name} />
                            </ListItem>
                        );
                    })}
                </List>
            </div>
        );

        if (['xs', 'sm'].includes(width)) {
            return (
                <nav className={classes.root}>
                    <Drawer
                        variant="temporary"
                        onClose={onClose}
                        classes={{
                            paper: classes.paper
                        }}
                        open={open}
                    >
                        {list}
                    </Drawer>
                </nav>
            );
        }
        return (
            <nav className={classes.root}>
                <Drawer
                    variant="permanent"
                    open
                    classes={{
                        paper: classes.paper
                    }}
                >
                    {list}
                </Drawer>
            </nav>
        );
    }
}

const mapStateToProps = (state) => ({
    title: getTitle(state),
    pathname: getPathname(state)
});

const actionCreators = {
};

export default withTheme(withUser(compose(
    withStyles((theme) => ({
        root: {
            [theme.breakpoints.up('md')]: {
                width: DRAWER_WIDTH,
                flexShrink: 0
            }
        },
        paper: {
            width: DRAWER_WIDTH
        },
        toolbar: theme.mixins.toolbar
    })),
    withWidth(),
    connect(mapStateToProps, actionCreators)
)(AppDrawer)));
