/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import {Route, Redirect} from 'react-router';
import withUser from '../hocs/withUser';

class UserRoute extends React.Component {
    static propTypes = {
        component: PropTypes.elementType.isRequired,
        isLoggedIn: PropTypes.bool.isRequired
    };

    render() {
        const {component: Component, isLoggedIn, ...rest} = this.props;
        return (
            <Route
                {...rest}
                render={(props) => (isLoggedIn ? <Component {...props} /> : <Redirect to="/login" />)}
            />
        );
    }
}

export default withUser(UserRoute);
