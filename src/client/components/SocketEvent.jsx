import React from 'react';
import PropTypes from 'prop-types';
import {SocketContext} from './Socket';

class SocketEvent extends React.Component {
    static contextType = SocketContext;

    static propTypes = {
        event: PropTypes.string.isRequired,
        handler: PropTypes.func.isRequired
    };

    componentDidMount() {
        const {event, handler} = this.props;
        const socket = this.context;

        if (!socket) {
            console.warn('Socket IO connection has not been established.');
            return;
        }

        socket.on(event, handler);
    }

    componentWillUnmount() {
        const {event, handler} = this.props;
        const socket = this.context;

        if (!socket) {
            console.warn('Socket IO connection has not been established.');
            return;
        }

        socket.off(event, handler);
    }

    render() {
        return false;
    }
}

export default SocketEvent;
