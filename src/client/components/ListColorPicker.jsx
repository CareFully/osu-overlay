/* eslint-disable no-unused-vars */
import React from 'react';
import {
    ListItemSecondaryAction,
    ListItemText,
    TextField
} from '@material-ui/core';
import PropTypes from 'prop-types';
import {ColorPicker} from 'material-ui-color';
import withDisableable from '../hocs/withDisableable';

class ListColorPicker extends React.Component {
    static propTypes = {
        label: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
        disabled: PropTypes.bool
    };

    static defaultProps = {
        disabled: false
    }

    constructor(props) {
        super(props);
        this.onValueChanged = this.onValueChanged.bind(this);
    }

    onValueChanged(value) {
        console.log('on color change', value);
        if (value.hex) {
            this.props.onChange(this.props.name, `#${value.hex}`);
        }
    }

    render() {
        const {
            label, name, value, disabled
        } = this.props;
        return (
            <>
                <ListItemText primary={label} />
                <ListItemSecondaryAction>
                    <ColorPicker hideTextfield variant="outlined" value={value} onChange={this.onValueChanged} />
                </ListItemSecondaryAction>
            </>
            // eslint-disable-next-line max-len
            // <TextField disabled={disabled} fullWidth type="text" label={label} size="small" variant="outlined" name={name} value={value} onChange={this.onValueChanged} />
        );
    }
}

export default withDisableable(ListColorPicker);
