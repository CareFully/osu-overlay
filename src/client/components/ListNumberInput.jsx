import React from 'react';
import {
    TextField
} from '@material-ui/core';
import PropTypes from 'prop-types';
import withDisableable from '../hocs/withDisableable';

class ListNumberInput extends React.Component {
    static propTypes = {
        label: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        value: PropTypes.number.isRequired,
        onChange: PropTypes.func.isRequired,
        disabled: PropTypes.bool
    };

    static defaultProps = {
        disabled: false
    }

    static getDerivedStateFromProps(props, state) {
        if (props.value !== state.prevValue) {
            return {
                localValue: props.value,
                prevValue: props.value
            };
        }
        return null;
    }

    constructor(props) {
        super(props);
        this.state = {
            localValue: props.value,
            prevValue: props.value
        };

        this.onValueChanged = this.onValueChanged.bind(this);
    }

    onValueChanged(event) {
        const {target} = event;
        const {name, value} = target;
        const numberValue = parseFloat(value);

        this.setState({
            localValue: value
        });

        if (!Number.isNaN(numberValue)) {
            this.setState({
                prevValue: numberValue
            });
            this.props.onChange(name, numberValue);
        }
    }

    render() {
        const {
            label, name, disabled
        } = this.props;
        const {
            localValue
        } = this.state;
        return (
            <TextField disabled={disabled} fullWidth type="text" label={label} size="small" variant="outlined" name={name} value={localValue} onChange={this.onValueChanged} />
        );
    }
}

export default withDisableable(ListNumberInput);
