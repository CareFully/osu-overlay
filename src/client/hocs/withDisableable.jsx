/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import {
    Checkbox, ListItem, ListItemIcon
} from '@material-ui/core';

export default (WrappedComponent) => {
    const Component = class extends React.Component {
        static propTypes = {
            name: PropTypes.string.isRequired,
            value: PropTypes.any,
            defaultValue: PropTypes.any,
            disableable: PropTypes.bool,
            onChange: PropTypes.func.isRequired
        };

        static defaultProps = {
            value: undefined,
            disableable: false,
            defaultValue: false
        }

        constructor(props) {
            super(props);
            this.onDisableableChanged = this.onDisableableChanged.bind(this);
        }

        onDisableableChanged(event) {
            const {
                name, defaultValue
            } = this.props;
            if (event.target.checked) {
                this.props.onChange(name, defaultValue);
            } else {
                this.props.onChange(name, undefined);
            }
        }

        render() {
            const {
                value, disableable, defaultValue
            } = this.props;
            const controlledValue = value === undefined ? defaultValue : value;

            return (
                <ListItem>
                    {disableable && (
                        <ListItemIcon>
                            <Checkbox
                                edge="start"
                                checked={value !== undefined}
                                onChange={this.onDisableableChanged}
                                disableRipple
                            />
                        </ListItemIcon>
                    )}
                    <WrappedComponent
                        {...{...this.props, value: controlledValue}}
                        disabled={value === undefined}
                    />
                </ListItem>
            );
        }
    };
    return Component;
};
