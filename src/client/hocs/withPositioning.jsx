/* eslint-disable max-len */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import {Spring} from 'react-spring/renderprops';
import {withStyles} from '@material-ui/core';

export const ANCHOR_TOP_LEFT = 0;
export const ANCHOR_TOP_RIGHT = 1;
export const ANCHOR_TOP_CENTER = 2;
export const ANCHOR_BOTTOM_LEFT = 3;
export const ANCHOR_BOTTOM_RIGHT = 4;
export const ANCHOR_BOTTOM_CENTER = 5;
export const ANCHOR_CENTER = 6;
export const ANCHORS = {
    'Top-left': ANCHOR_TOP_LEFT,
    'Top-right': ANCHOR_TOP_RIGHT,
    'Top-center': ANCHOR_TOP_CENTER,
    'Bottom-left': ANCHOR_BOTTOM_LEFT,
    'Bottom-right': ANCHOR_BOTTOM_RIGHT,
    'Bottom-center': ANCHOR_BOTTOM_CENTER,
    Center: ANCHOR_CENTER
};

export default (WrappedComponent) => {
    const Component = class extends React.Component {
        static propTypes = {
            classes: PropTypes.object.isRequired,
            show: PropTypes.bool,
            width: PropTypes.number,
            height: PropTypes.number,
            widthUnit: PropTypes.string,
            heightUnit: PropTypes.string,
            x: PropTypes.number,
            y: PropTypes.number,
            angle: PropTypes.number,
            anchor: PropTypes.oneOf([
                ANCHOR_TOP_LEFT,
                ANCHOR_TOP_RIGHT,
                ANCHOR_TOP_CENTER,
                ANCHOR_BOTTOM_LEFT,
                ANCHOR_BOTTOM_RIGHT,
                ANCHOR_BOTTOM_CENTER,
                ANCHOR_CENTER
            ]),
            onClick: PropTypes.func,
            editing: PropTypes.bool
        };

        static defaultProps = {
            show: true,
            width: 100,
            height: 10,
            widthUnit: 'vw',
            heightUnit: 'vh',
            x: 0,
            y: 0,
            angle: 0,
            onClick: null,
            anchor: ANCHOR_TOP_LEFT,
            editing: false
        }

        render() {
            const {
                classes,
                show,
                width,
                height, onClick, x, y, widthUnit, heightUnit, anchor, angle, editing, ...rest
            } = this.props;

            let transform = `rotate(${angle}deg)`;

            switch (anchor) {
            case ANCHOR_CENTER:
                transform += ' translate(-50%,-50%)';
                break;
            case ANCHOR_TOP_RIGHT:
                transform += ' translate(-100%, 0%)';
                break;
            case ANCHOR_TOP_CENTER:
                transform += ' translate(-50%, 0%)';
                break;
            case ANCHOR_BOTTOM_LEFT:
                transform += ' translate(-0%, -100%)';
                break;
            case ANCHOR_BOTTOM_RIGHT:
                transform += ' translate(-100%, -100%)';
                break;
            case ANCHOR_BOTTOM_CENTER:
                transform += ' translate(-50%, -100%)';
                break;
            case ANCHOR_TOP_LEFT:
            default:
                transform += ' translate(0%, 0%)';
                break;
            }

            return (
                <Spring
                    from={{opacity: 0}}
                    to={{
                        opacity: show ? 1 : 0,
                        width: `${width}${widthUnit}`,
                        height: `${height}${heightUnit}`,
                        left: `${x}%`,
                        top: `${y}%`,
                        transform: transform
                    }}
                >
                    {(props) => (
                        // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                        <div
                            className={`${classes.root} ${editing ? classes.editing : ''}`}
                            style={{...props, opacity: (editing && props.opacity < 0.4 ? 0.4 : props.opacity)}}
                            onClick={onClick}
                        >
                            <WrappedComponent {...rest} />
                        </div>
                    )}
                </Spring>
            );
        }
    };
    return withStyles({
        root: {
            position: 'absolute'
        },
        editing: {
            margin: '-1px',
            border: '1px solid red'
        }
    })(Component);
};
