import bodyParser from 'body-parser';
import express from 'express';
import path from 'path';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUI from 'swagger-ui-express';
import 'dotenv/config';
import './utils/db';
import {createServer} from 'http';
import {Server} from 'socket.io';
import routes from './routes';

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'care-react-boilerplate',
            version: '1.0.0'
        },
        servers: [{
            url: '/api'
        }]
    },
    apis: ['./src/server/docs/*.yaml', './src/server/routes/*.js', './src/server/models/*.js']
};

const swaggerDocument = swaggerJSDoc(options);

const app = express();
const server = createServer(app);
const io = new Server(server);
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(express.static('dist', {
    maxAge: '1w'
}));

app.use('/api', routes);

app.use('/api/docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));
app.use('/api/docs.json', (req, res) => {
    res.json(swaggerDocument);
});

app.get('/*', (req, res) => {
    res.sendFile(path.join(process.cwd(), 'dist/index.html'), (err) => {
        if (err) {
            res.status(500).send(err);
        }
    });
});

app.set('socket', io);

io.on('connection', (socket) => {
    console.log('websocket connected', socket.id);
    socket.on('disconnect', () => {
        console.log('websocket disconnected', socket.id);
    });
});

server.listen(port, () => {
    console.log(`Server started on port ${port}`);
});
