/* eslint-disable func-names */
import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

/**
 * @swagger
 * components:
 *   schemas:
 *     ArrayOfUsers:
 *       type: array
 *       items:
 *         $ref: '#/components/schemas/User'
 *     User:
 *       type: object
 *       properties:
 *         _id:
 *           type: string
 *           example: 5da9419676e6b80fbcafa336
 *           readOnly: true
 *         firstname:
 *           type: string
 *           example: Foo
 *         lastname:
 *           type: string
 *           example: Bar
 *         email:
 *           type: string
 *           example: foo@bar.com
 *         password:
 *           type: string
 *           writeOnly: true
 *           example: P2$$w0rd
 *         admin:
 *           type: boolean
 *           readOnly: true
 *           example: false
 *         createdAt:
 *           type: string
 *           format: date-time
 *           example: 2019-10-18T04:37:42.964Z
 *           readOnly: true
 *         updatedAt:
 *           type: string
 *           format: date-time
 *           example: 2019-10-18T04:37:42.964Z
 *           readOnly: true
 *         __v:
 *           type: integer
 *           example: 0
 *           readOnly: true
 */
const Password = new mongoose.Schema({
    hash: {
        type: String,
        required: true
    }
});

const User = new mongoose.Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: Password,
        required: true
    },
    oldPasswords: [{
        hash: {
            type: String,
            required: true
        },
        createdAt: {
            type: Date,
            default: Date.now
        }
    }],
    admin: {
        type: Boolean,
        required: true,
        default: false
    }
}, {timestamps: true});

User.post('init', function () {
    this.originalPassword = this.password;
});

User.pre('save', async function () {
    if (this.modifiedPaths().includes('password')) {
        this.password.hash = bcrypt.hashSync(this.password.hash, 10);
        if (this.originalPassword) {
            this.oldPasswords.push(this.originalPassword);
        }
    }
});

User.set('toJSON', {
    transform: (doc, ret) => ({
        _id: ret._id,
        firstname: ret.firstname,
        lastname: ret.lastname,
        email: ret.email,
        admin: ret.admin
    })
});

export default mongoose.model('User', User);
