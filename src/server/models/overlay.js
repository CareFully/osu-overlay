/* eslint-disable prefer-arrow-callback */
/* eslint-disable func-names */
import mongoose from 'mongoose';

export const OVERLAY_TYPE_TEXT = 'text';
export const OVERLAY_TYPE_IMAGE = 'image';
export const OVERLAY_TYPE_FRAME = 'frame';
export const OVERLAY_TYPES = [
    OVERLAY_TYPE_TEXT,
    OVERLAY_TYPE_IMAGE,
    OVERLAY_TYPE_FRAME
];
export const SIZE_UNITS = ['px', '%'];

const sharedOptions = {
    show: {
        type: Boolean
    },
    x: {
        type: Number
    },
    y: {
        type: Number
    },
    width: {
        type: Number
    },
    height: {
        type: Number
    },
    angle: {
        type: Number
    },
    widthUnit: {
        type: String,
        enum: SIZE_UNITS
    },
    heightUnit: {
        type: String,
        enum: SIZE_UNITS
    },
    anchor: {
        type: Number,
        min: 0,
        max: 6
    }
};

const overlaySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: OVERLAY_TYPES,
        default: OVERLAY_TYPE_TEXT
    },
    inGame: {
        type: Boolean,
        default: true
    },
    inMenu: {
        type: Boolean,
        default: true
    },
    onDesktop: {
        type: Boolean,
        default: true
    },
    active: {
        type: Boolean,
        default: true
    },
    order: {
        type: Number
    },
    options: sharedOptions,
    hiddenOptions: sharedOptions
}, {
    timestamps: true,
    discriminatorKey: 'type'
});

export const Overlay = mongoose.model('Overlay', overlaySchema);

export const textOverlay = Overlay.discriminator(OVERLAY_TYPE_TEXT, new mongoose.Schema({
    options: {
        text: {
            type: String,
            required: true
        },
        background: {
            type: String
        },
        elevation: {
            type: Number
        },
        border: {
            type: Number
        },
        textSize: {
            type: Number
        },
        textAlign: {
            type: Number,
            min: 0,
            max: 2
        },
        textColor: {
            type: String
        },
        textElevation: {
            type: Number
        }
    },
    hiddenOptions: {
        text: {
            type: String
        },
        background: {
            type: String
        },
        elevation: {
            type: Number
        },
        border: {
            type: Number
        },
        textSize: {
            type: Number
        },
        textAlign: {
            type: Number,
            min: 0,
            max: 2
        },
        textColor: {
            type: String
        },
        textElevation: {
            type: Number
        }
    }
}, {
    timestamps: true,
    discriminatorKey: 'type'
}));

export const imageOverlay = Overlay.discriminator(OVERLAY_TYPE_IMAGE, new mongoose.Schema({
    options: {
        url: {
            type: String,
            required: true
        },
        imageKey: {
            type: String
        }
    },
    hiddenOptions: {
        url: {
            type: String
        },
        imageKey: {
            type: String
        }
    }
}));

export const frameOverlay = Overlay.discriminator(OVERLAY_TYPE_FRAME, new mongoose.Schema({
    options: {
        url: {
            type: String,
            required: true
        }
    },
    hiddenOptions: {
        url: {
            type: String
        }
    }
}));
