/* eslint-disable newline-per-chained-call */
import {body, param} from 'express-validator';
import {
    OVERLAY_TYPES, OVERLAY_TYPE_IMAGE, OVERLAY_TYPE_TEXT, SIZE_UNITS
} from '../models/overlay';

const addOption = (name, validator, defaultValue) => [
    validator(body(`options.${name}`).default(defaultValue)),
    validator(body(`hiddenOptions.${name}`).optional())
];

const OVERLAY_ID = () => param('id').isMongoId();
const OVERLAY = () => [
    body('name', 'Name is required').isString().isLength({min: 1}),
    body('type', 'Type is required').isIn(OVERLAY_TYPES),
    body('active').default(true).isBoolean(),
    body('inGame').default(true).isBoolean(),
    body('inMenu').default(true).isBoolean(),
    body('onDesktop').default(true).isBoolean(),
    ...addOption('show', (validator) => validator.isBoolean(), true),
    ...addOption('x', (validator) => validator.isNumeric(), 0),
    ...addOption('y', (validator) => validator.isNumeric(), 0),
    ...addOption('width', (validator) => validator.isNumeric(), 100),
    ...addOption('height', (validator) => validator.isNumeric(), 100),
    ...addOption('angle', (validator) => validator.isNumeric(), 0),
    ...addOption('widthUnit', (validator) => validator.isIn(SIZE_UNITS), 'px'),
    ...addOption('heightUnit', (validator) => validator.isIn(SIZE_UNITS), 'px'),
    ...addOption('anchor', (validator) => validator.isNumeric().isInt({min: 0, max: 6}), 0),

    ...addOption('text', (validator) => validator.if(body('type').equals(OVERLAY_TYPE_TEXT)).isString(), 'Text'),
    ...addOption('background', (validator) => validator.if(body('type').equals(OVERLAY_TYPE_TEXT)).isString(), '#00000000'),
    ...addOption('elevation', (validator) => validator.if(body('type').equals(OVERLAY_TYPE_TEXT)).isInt({min: 0}), 0),
    ...addOption('border', (validator) => validator.if(body('type').equals(OVERLAY_TYPE_TEXT)).isInt({min: 0}), 0),
    ...addOption('textSize', (validator) => validator.if(body('type').equals(OVERLAY_TYPE_TEXT)).isNumeric(), 16),
    ...addOption('textAlign', (validator) => validator.if(body('type').equals(OVERLAY_TYPE_TEXT)).isInt({min: 0, max: 2}), 0),
    ...addOption('textColor', (validator) => validator.if(body('type').equals(OVERLAY_TYPE_TEXT)).isString({min: 0}), '#ffffff'),
    ...addOption('textElevation', (validator) => validator.if(body('type').equals(OVERLAY_TYPE_TEXT)).isInt({min: 0}), 0),

    ...addOption('url', (validator) => validator.if(body('type').equals(OVERLAY_TYPE_IMAGE)).isString(), 'https://'),
    ...addOption('imageKey', (validator) => validator.if(body('type').equals(OVERLAY_TYPE_IMAGE)).isString(), '')
];

export default {
    overlay: [
        ...OVERLAY()
    ],
    overlayUpdate: [
        ...OVERLAY()
    ],
    overlayId: [
        OVERLAY_ID()
    ],
    setOrder: [
        OVERLAY_ID(),
        body('order').isInt({min: 0})
    ]
};
