/* eslint-disable newline-per-chained-call */
import {body} from 'express-validator';
import Password from '../services/password';

const NEW_PASSWORD = () => [
    body('password', 'Password is required')
        .isString()
        .isLength({min: 6}).withMessage('Password must be at least 6 characters long'),
    body('confirmPassword', 'Password confirmation is required')
        .isString()
        .custom((value, {req}) => {
            if (value !== req.body.password) {
                throw new Error('Passwords don\'t match');
            }
            return true;
        })
];

export default {
    email: [
        body('email', 'Email is required')
            .isString()
            .isEmail().withMessage('Invalid email')
            .normalizeEmail()
    ],
    login: [
        body('email').isString().trim(),
        body('password').isString()
    ],
    newPassword: [
        body('token', 'Invalid reset token').isJWT(),
        ...NEW_PASSWORD()
    ],
    changePassword: [
        body('oldPassword', 'You must enter your old password')
            .isString()
            .custom(async (value, {res}) => {
                const user = res.locals;
                console.log(user);
                if (Password.verify(user, value)) {
                    throw new Error('Invalid old password');
                }
                return true;
            }),
        ...NEW_PASSWORD()
    ]
};
