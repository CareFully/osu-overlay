/* eslint-disable class-methods-use-this */
/* eslint-disable no-underscore-dangle */
import ModelController from './model';
import {Overlay} from '../models/overlay';

class OverlayController extends ModelController {
    constructor() {
        super(Overlay);
    }

    async getAll(req, res) {
        res.json(
            (await this.Model.find({})
                .sort({order: 1}))
                .map((doc) => doc.toObject({minimize: false}))
        );
    }

    async create(req, res) {
        console.log(req.body);
        try {
            const count = await Overlay.countDocuments();
            const doc = new this.Model({
                ...req.body,
                order: count
            });
            await doc.save();
            req.app.get('socket').emit('overlay/update');
            return res.status(201).json({
                message: `${this.modelName} created`
            });
        } catch (err) {
            return res.status(400).json({
                message: err.message
            });
        }
    }

    async update(req, res) {
        console.log(req.body);
        const overlay = await Overlay.findById(req.params.id);
        if (overlay) {
            overlay.name = req.body.name;
            overlay.type = req.body.type;
            overlay.active = req.body.active;
            overlay.options = req.body.options;
            overlay.hiddenOptions = req.body.hiddenOptions;
            overlay.inGame = req.body.inGame;
            overlay.inMenu = req.body.inMenu;
            overlay.onDesktop = req.body.onDesktop;
            await overlay.save();
            req.app.get('socket').emit('overlay/update');
            return res.json({
                message: 'Overlay updated',
                overlay: overlay
            });
        }
        return res.status(404).json({
            message: 'Did not find an overlay with given id'
        });
    }

    async setOrder(req, res) {
        const newOrder = req.body.order;
        const overlay = await Overlay.findById(req.params.id);
        const count = await Overlay.countDocuments();
        if (newOrder >= count) {
            return res.status(400).json({
                message: 'Order number too large'
            });
        }
        if (overlay) {
            const oldOrder = overlay.order;
            overlay.order = newOrder;
            const promises = [overlay.save()];
            if (newOrder > oldOrder) {
                const overlays = await Overlay.find({
                    $and: [
                        {order: {$gt: oldOrder}},
                        {order: {$lte: newOrder}}
                    ]
                });
                for (let i = 0; i < overlays.length; i++) {
                    const doc = overlays[i];
                    doc.order--;
                    promises.push(doc.save());
                }
            } else if (newOrder < oldOrder) {
                const overlays = await Overlay.find({
                    $and: [
                        {order: {$gte: newOrder}},
                        {order: {$lt: oldOrder}}
                    ]
                });
                for (let i = 0; i < overlays.length; i++) {
                    const doc = overlays[i];
                    doc.order++;
                    promises.push(doc.save());
                }
            }
            await Promise.all(promises);
            req.app.get('socket').emit('overlay/update');
            return res.json({
                message: 'Order updated'
            });
        }
        return res.status(404).json({
            message: 'Did not find an overlay with given id'
        });
    }

    async delete(req, res) {
        const overlay = await this.Model.findById(req.params.id);
        if (overlay) {
            const overlays = await Overlay.find({order: {$gt: overlay.order}});
            const promises = [overlay.remove()];
            for (let i = 0; i < overlays.length; i++) {
                const doc = overlays[i];
                doc.order--;
                promises.push(doc.save());
            }
            await Promise.all(promises);
        }
        req.app.get('socket').emit('overlay/update');
        res.json({
            message: `${this.modelName} deleted`
        });
    }
}

export default new OverlayController();
