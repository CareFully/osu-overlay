/* eslint-disable class-methods-use-this */
import bcrypt from 'bcrypt';

class Password {
    verify(user, password) {
        return bcrypt.compareSync(password, user.password.hash);
    }

    async isUsedPassword(user, password) {
        if (this.verify(user, password)) {
            return true;
        }
        const promises = [];
        user.oldPasswords.forEach((old) => {
            promises.push(bcrypt.compare(password, old.hash));
        });
        const results = await Promise.all(promises);
        if (results.includes(true)) {
            return true;
        }
        return false;
    }
}

export default new Password();
