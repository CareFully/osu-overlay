import express from 'express';
import validation from '../middleware/validation';
import authorization from '../middleware/authorization';
import overlaySchemas from '../schemas/overlay';
import OverlayController from '../controllers/overlay';

const router = express.Router();

router.get('/', (req, res) => OverlayController.getAll(req, res));
router.post('/', validation(overlaySchemas.overlay), (req, res) => OverlayController.create(req, res));
router.delete('/', authorization(true), (req, res) => OverlayController.deleteAll(req, res));

router.get('/:id', validation(overlaySchemas.overlayId), (req, res) => OverlayController.get(req, res));
router.put('/:id/order', validation(overlaySchemas.setOrder), (req, res) => OverlayController.setOrder(req, res));
router.put('/:id', validation(overlaySchemas.overlayUpdate), (req, res) => OverlayController.update(req, res));
router.delete('/:id', validation(overlaySchemas.overlayId), (req, res) => OverlayController.delete(req, res));

export default router;
