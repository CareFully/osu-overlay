/* eslint-disable no-underscore-dangle */
import express from 'express';
import SampleController from '../controllers/sample';

const router = express.Router();

router.get('/', (req, res) => SampleController.getFoo(req, res));

export default router;
