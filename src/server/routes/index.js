/* eslint-disable no-underscore-dangle */
import express from 'express';
import userRoutes from './users';
import authRoutes from './auth';
import sampleRoutes from './sample';
import overlayRoutes from './overlay';

const router = express.Router();

router.use('/foo', sampleRoutes);
router.use('/user', userRoutes);
router.use('/auth', authRoutes);
router.use('/overlay', overlayRoutes);

export default router;
