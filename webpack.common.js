/* eslint-disable */
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');
require('dotenv').config()

module.exports = {
    entry: './src/client/index.jsx',
    output: {
        filename: 'assets/js/[name].[contenthash].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
    },
    plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: 'assets/css/[name].[contenthash].css'
        }),
        new Dotenv(),
        new HtmlWebpackPlugin({
            inject: false,
            template: require('html-webpack-template'),
            title: process.env.APP_NAME,
            bodyHtmlSnippet: '<div id="app"></div>',
            links: [
                'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap',
                'https://fonts.googleapis.com/icon?family=Material+Icons'
            ],
            mobile: true
        })
    ],
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'eslint-loader'
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.(sa|sc|c)ss$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'assets/images/[contenthash].[ext]'
                        }
                    }
                ]
            },
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx', '.scss', '.css']
    },
    optimization: {
        moduleIds: 'hashed',
        runtimeChunk: 'single'
    }
};